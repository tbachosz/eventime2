package com.eventime.eventime.screens.events;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eventime.eventime.R;
import com.eventime.eventime.model.Event;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.eventime.eventime.utils.Constants.loggedUser;
import static com.eventime.eventime.utils.TimeHandler.parseDateTime;

public class EventViewFragment extends Fragment {
    public static final String TAG = EventViewFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;

    private CompositeSubscription mSubscriptions;

    private Event thisEvent;
    private int eventPosition;
    private boolean boolJoined, boolInterested;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager mPager;
    @BindView(R.id.edit_event)
    Button bEditEvent;
    @BindView(R.id.tv_name)
    TextView tvEventName;
    @BindView(R.id.tv_type)
    TextView tvEventType;
//    @BindView(R.id.tv_topic)
//    TextView tvEventTopic;
    @BindView(R.id.tv_host)
    TextView tvEventHost;
//    @BindView(R.id.tv_users_disclaimer)
//    TextView tvUsersDisclaimer;
    @BindView(R.id.tv_description)
    TextView tvDescription;
//    @BindView(R.id.tv_description)
//    TextView tvDescription;
//    @BindView(R.id.b_users)
//    TextView bUsersAttending;
    @BindView(R.id.tv_start_date)
TextView tvStartDate;
    @BindView(R.id.imb_join)
    ImageButton imbJoin;
    @BindView(R.id.imb_map)
    ImageButton imbOpenMap;
    @BindView(R.id.iv_event_image)
    ImageView ivEventImage;
    @BindView(R.id.imb_follow)
    ImageButton imbFollow;
    @BindView(R.id.iv_host_image)
    ImageView ivHostImage;
//    @BindView(R.id.tv_address)
//    TextView tvAddress;


    @OnClick(R.id.edit_event)
    public void moveToEditEvent(){
        replaceFragment(1);
    }
//    @OnClick(R.id.tv_users_disclaimer)
//    public void showEventsUsers(){
//        replaceFragment(2);
//    }

    @OnClick(R.id.imb_map)
    public void showMap(){
        replaceFragment(3);
    }


    public static EventViewFragment newInstance(String param1, String param2) {
        EventViewFragment fragment = new EventViewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public EventViewFragment() {
        // Required empty public constructor
    }

    @OnClick(R.id.imb_follow)
    public void onClickFollow(){
        Log.e(TAG,boolInterested+"");
        if(boolInterested==false){
            boolInterested=true;
            Toast.makeText(getActivity(),"You are following this event", Toast.LENGTH_LONG).show();
            imbFollow.setBackground(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.ic_event_busy_white_48dp, null));
            interestedEvent();
        }else{
            boolInterested=false;
            Toast.makeText(getActivity(),"You have unfollowed this event", Toast.LENGTH_LONG).show();
            imbFollow.setBackground(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.ic_event_white_48dp, null));
            uninterestedEvent();
        }

    }

    @OnClick(R.id.imb_join)
    public void onClickJoin(){
        Log.e(TAG, boolJoined+"");
        if(boolJoined==false){
            boolJoined = true;
            Toast.makeText(getActivity(),"You have joined this event", Toast.LENGTH_LONG).show();
            imbJoin.setBackground(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.ic_event_busy_white_48dp, null));
            joinEvent();
        }else{
            boolJoined =false;
            unjoinEvent();
            Toast.makeText(getActivity(),"You have unjoined this event", Toast.LENGTH_LONG).show();
            imbJoin.setBackground(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.ic_event_available_white_48dp, null));
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_view, container, false);
        ButterKnife.bind(this,view);
        thisEvent = getArguments().getParcelable("event");
        eventPosition = getArguments().getInt("position");

        Log.e("Event view", thisEvent.toString());
        tvDescription.setText(thisEvent.getDescription());
        tvEventName.setText(thisEvent.getName());
        tvEventType.setText("("+tvEventType.getText().toString()+" "+thisEvent.getType()+")");

//        tvUsersDisclaimer.setText(tvUsersDisclaimer.getText().toString()+ " " +thisEvent.getTotalOfUsersInterested());
//        if(!Constants.loggedUser.getEmail().equals(thisEvent.getHost())){
//            bEditEvent.setVisibility(View.GONE);
//            imbJoin.setVisibility(View.GONE);
//            imbFollow.setVisibility(View.GONE);
//        }
        if(thisEvent.getImage() != null)
            Picasso.with(EventViewFragment.this.getContext()).load(thisEvent.getImage()).into(ivEventImage);

        tvStartDate.setText(getContext().getResources().getString(R.string.start_date_item)+ " " + parseDateTime(thisEvent.getStartTime()));
//        tvAddress.setText(thisEvent.getAddress());

        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.description)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.comments)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

//        mPager.setAdapter(new EventViewFragment.PagerAdapter(getChildFragmentManager()));
//        tabLayout.setupWithViewPager(mPager);

        String picture;
        tvEventHost.setText(thisEvent.getHost().getName());
        picture = thisEvent.getHost().getPicture();

        if(picture!=null){
            Picasso.with(EventViewFragment.this.getContext()).load(picture).into(ivHostImage);
        }

//        boolJoined=isUserInEvent();
//        boolInterested=isUserFollowingEvent();
        Log.e(TAG + "OnCreate", boolJoined+"");
        Log.e(TAG + "OnCreate", boolInterested+"");
//        else{
//            ResourcesCompat.getDrawable(getContext().getResources(), R.mipmap.ic_default_picture_event, null);
//        }
        return view;
    }

//    public boolean isUserInEvent(){
//        List<String> usersIn = thisEvent.getUsers_attending();
//        String username = loggedUser.getName();
//        for(int i = 0; i<usersIn.size();i++){
//            if(username==usersIn.get(i)){
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean isUserFollowingEvent(){
//        List<String> usersIn = thisEvent.getUsers_interested();
//        String username = loggedUser.getName();
//        for(int i = 0; i<usersIn.size();i++){
//            if(username==usersIn.get(i)){
//                return true;
//            }
//        }
//        return false;
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    public static EventViewFragment getInstance(){
        return new EventViewFragment();
    }

    private void replaceFragment(int code) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putParcelable("event",thisEvent);

        ft.addToBackStack("EventViewFragment");

        if(code==0){
            EventViewFragment fragment = new EventViewFragment();
            ft.replace(R.id.fragmentFrame, fragment, EventViewFragment.TAG);
        }
        else if(code==1){
            EventEditFragment fragment = new EventEditFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, EventEditFragment.TAG);
        }
        else if(code==2){
//            UsersTabFragment fragment = new UsersTabFragment();
//            fragment.setArguments(bundle);
//            ft.replace(R.id.fragmentFrame, fragment, UsersTabFragment.TAG);
        }else if(code==3){
            MapFragment fragment = new MapFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, MapFragment.TAG);
        }
        ft.commit();
    }

    private void showSnackBarMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private void joinEvent() {
//        thisEvent.getUsers_attending().add(loggedUser.getEmail());
//        EventsFragment.eventList.get(eventPosition).getUsers_attending().add(loggedUser.getEmail());
//        if(EventsOnMapFragment.eventList != null)
//            EventsOnMapFragment.eventList.get(eventPosition).getUsers_attending().add(loggedUser.getEmail());

        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).joinEvent(thisEvent.getId(), loggedUser.getEmail()) // these info get from user
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseJoin, this::handleError));
    }

    private void handleResponseJoin(Response response) {
        if(response.getToken() != null)
            Constants.saveAccessToken(getActivity(), response.getToken());


        Log.e(TAG, "Joined!:");
        showSnackBarMessage(getString(R.string.event_joined));
    }


    private void unjoinEvent() {
//        thisEvent.getUsers_attending().remove(loggedUser.getEmail());
//        EventsFragment.eventList.get(eventPosition).getUsers_attending().remove(loggedUser.getEmail());
//        if(EventsOnMapFragment.eventList != null)
//            EventsOnMapFragment.eventList.get(eventPosition).getUsers_attending().remove(loggedUser.getEmail());

        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).unjoinEvent(thisEvent.getId(), loggedUser.getEmail()) // these info get from user
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseUnjoin, this::handleError));
    }

    private void handleResponseUnjoin(Response response) {
        if(response.getToken() != null)
            Constants.saveAccessToken(getActivity(), response.getToken());


        Log.e(TAG, "Unjoined!:");
        showSnackBarMessage(getString(R.string.event_unjoined));
    }


    private void interestedEvent() {
//        thisEvent.getUsers_interested().add(loggedUser.getEmail());
//        EventsFragment.eventList.get(eventPosition).getUsers_interested().add(loggedUser.getEmail());
//        if(EventsOnMapFragment.eventList != null)
//            EventsOnMapFragment.eventList.get(eventPosition).getUsers_interested().add(loggedUser.getEmail());

        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).interestedinEvent(thisEvent.getId(), loggedUser.getEmail()) // these info get from user
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseInterested, this::handleError));
    }

    private void handleResponseInterested(Response response) {
        if(response.getToken() != null)
            Constants.saveAccessToken(getActivity(), response.getToken());


        Log.e(TAG, "Interested!:");
        showSnackBarMessage(getString(R.string.event_interested));
    }

    private void uninterestedEvent() {
//        thisEvent.getUsers_interested().remove(loggedUser.getEmail());
//        EventsFragment.eventList.get(eventPosition).getUsers_interested().remove(loggedUser.getEmail());
//        if(EventsOnMapFragment.eventList != null)
//            EventsOnMapFragment.eventList.get(eventPosition).getUsers_interested().remove(loggedUser.getEmail());

        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).uninterestedinEvent(thisEvent.getId(), loggedUser.getEmail()) // these info get from user
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseUninterested, this::handleError));
    }

    private void handleResponseUninterested(Response response) {
        if(response.getToken() != null)
            Constants.saveAccessToken(getActivity(), response.getToken());

        Log.e(TAG, "Uninterested!:");
        showSnackBarMessage("Event unfollowed");
    }


//    private void getUserPicture(String email) {
//
//        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).getPicture(email) // these info get from user
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(this::handleResponseUserPicture, this::handleError));
//    }
//
//    private void handleResponseUserPicture(User user) {
//        if(user.getToken() != null)
//            Constants.saveAccessToken(getActivity(), user.getToken());
//
//        Log.e(TAG, "User data: " + user.getName());
//        String picture;
//        if(thisEvent.getHostUser()!= null){
//            tvEventHost.setText(thisEvent.getHostUser().getName());
//            picture = thisEvent.getHostUser().getPicture();
//        }else if(thisEvent.getHostOrganization()!=null){
//            tvEventHost.setText(thisEvent.getHostOrganization().getName());
//            picture = thisEvent.getHostOrganization().getPicture();
//        }else{
//            tvEventHost.setText(thisEvent.getHostPolitician().getName());
//            picture = thisEvent.getHostPolitician().getPicture();
//        }
//        tvEventHost.setText(user.getName()); //tvEventHost.getText().toString()+ " " +
//
//        //getUserPicture(thisEvent.getHost());
//
//        if(picture!=null){
//            Picasso.with(EventViewFragment.this.getContext()).load(picture).into(ivHostImage);
//        }
//        else{
////            ResourcesCompat.getDrawable(getContext().getResources(), R.mipmap.ic_default_picture_event, null);
//        }
//
//    }

    private void handleError(Throwable error) {
        Log.e(TAG, "Error: " + error.getMessage());

        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();

            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            showSnackBarMessage("Network Error !");
        }
    }
}
