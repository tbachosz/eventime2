package com.eventime.eventime.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eventime.eventime.R;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.model.User;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.screens.events.EventsFragment;
import com.eventime.eventime.screens.events.EventsOnMapFragment;
import com.eventime.eventime.screens.users.ChangePasswordDialog;
import com.eventime.eventime.screens.users.EditProfileFragment;
import com.eventime.eventime.screens.users.LogInActivity;
import com.eventime.eventime.utils.Constants;
import com.facebook.AccessToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import static com.eventime.eventime.screens.events.EventsFragment.eventList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Tomek on 2018-01-03.
 */

public class MainActivity extends AppCompatActivity implements ChangePasswordDialog.Listener {

    public static final String TAG = MainActivity.class.getSimpleName();

    private CompositeSubscription mSubscriptions;

    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.DrawerLayout)
    DrawerLayout drawer;
    @BindView(R.id.pager)
    ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSubscriptions = new CompositeSubscription();
        ButterKnife.bind(this);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Events"));
        tabLayout.addTab(tabLayout.newTab().setText("My Politic"));
//        tabLayout.addTab(tabLayout.newTab().setText("Debates"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        ViewPager mPager = (ViewPager) findViewById(R.id.pager);

        //Strop refrehing
        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(2); /* limit is a fixed integer*/

        mPager.setAdapter(new PagerAdapter(this.getSupportFragmentManager()));
        tabLayout.setupWithViewPager(mPager);
        initDrawer();
        FragmentManager fm = this.getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            this.getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onPasswordChanged() {
        showSnackBarMessage(getString(R.string.change_success));
    }

    private void showSnackBarMessage(String message) {
        Snackbar.make(findViewById(R.id.fragmentFrame),message,Snackbar.LENGTH_SHORT).show();
    }

    private class PagerAdapter extends FragmentPagerAdapter {
        PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
//            switch (position) {
//                case 0:
//                    return EventsFragment.getInstance();
////                case 1:
////                    return PoliticFragment.getInstance();
////                case 2:
////                    return DebatesFragment.getInstance();
//                default: EventsFragment.getInstance();
//            }

//            return position == 0 ? EventsFragment.getInstance();
//                    : PoliticFragment.getInstance();
            return EventsFragment.getInstance();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getResources().getString(R.string.events);
//            switch (position) {
//                case 0:
//                    return getResources().getString(R.string.events);
//                case 1:
//                    return getResources().getString(R.string.my_politics);
//                default: return getResources().getString(R.string.events);
//                case 2:
//                    return getString(R.string.debates);
//                default: return getResources().getString(R.string.events);
//            }
//            return position == 0 ?  getResources().getString(R.string.events) :  getResources().getString(R.string.my_politics);
        }
    }

    public void initDrawer() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(false);
        View headerView = navigationView.getHeaderView(0);
        ImageView ivImage = (ImageView) headerView.findViewById(R.id.cv_image);
        TextView tvName = (TextView) headerView.findViewById(R.id.tv_name);
        tvName.setText(Constants.loggedUser.getName());
        if(Constants.loggedUser.getPicture()!=null){
            Picasso.with(getBaseContext()).load(Constants.loggedUser.getPicture()).into(ivImage);
        }
        Menu menu = navigationView.getMenu();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.main_menu:
                        replaceFragment(1);
                        Log.e(TAG, "my profile");
                        break;
                    case R.id.events_map:
                        replaceFragment(2);
                        Log.e(TAG,"map view");
                        break;
//                    case R.id.nav_chat:
//                        replaceFragment(3);
//                        Log.e(TAG,"chat");
//                        break;
                    case R.id.sign_out:
                        logout();
                        break;
                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    private void logout() {
        Constants.resetTokens(this);

        if(AccessToken.getCurrentAccessToken()!=null){
            AccessToken.setCurrentAccessToken(null);
        }
//        finish();
        Log.e(TAG,getResources().getString(R.string.user_log_out));

        logoutProcess(Constants.loggedUser.getEmail());
    }


    private void logoutProcess(String email) {
//        FCMtoken = FirebaseInstanceId.getInstance().getToken();
//        Log.d(TAG, "FCM token: " + FCMtoken);
        String FCMtoken = "temp";
        mSubscriptions.add(NetworkUtil.getRetrofit().logout(email, new User(FCMtoken))//Adding a dummy user to send FCM (otherwise, we cannot send strings)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }

    private void handleResponse(Response response) {

        Log.e(TAG, "Logout succeeded!: " + response.toString());

        startActivity(new Intent(this, LogInActivity.class));
    }

    private void handleError(Throwable error) {

        Log.e(TAG, "Logout error!: " + error.getMessage());

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void replaceFragment(int code) {
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        if (code == 1) {
            EditProfileFragment fragment = new EditProfileFragment();
            ft.replace(R.id.fragmentFrame, fragment, EventsFragment.TAG);
        } else if (code == 2) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("event_list", eventList);
            ft.addToBackStack("LogInFragment");
            EventsOnMapFragment fragment = new EventsOnMapFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, EventsOnMapFragment.TAG);
            ft.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_drawer, menu);
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void onBackPressed() {
        // Catch back action and pops from backstack
        // (if you called previously to addToBackStack() in your transaction)
        Log.e("Stack count", getSupportFragmentManager().getBackStackEntryCount() + "");
        if (getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStack();
        }
        // Default action on back pressed
        else {
            super.onBackPressed();
            this.finishAffinity();
        }
    }

}
