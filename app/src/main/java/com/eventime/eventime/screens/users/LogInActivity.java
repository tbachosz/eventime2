package com.eventime.eventime.screens.users;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.eventime.eventime.R;
import com.eventime.eventime.utils.Util;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class LogInActivity extends AppCompatActivity  implements ResetPasswordDialog.Listener{

    public static final String TAG = LogInActivity.class.getSimpleName();
    // The TransferUtility is the primary class for managing transfer to S3
    private TransferUtility transferUtility;

    public static Bitmap picBitmap;

    // The SimpleAdapter adapts the data about transfers to rows in the UI
    private SimpleAdapter simpleAdapter;

    // A List of all transfers
    private List<TransferObserver> observers;

    /**
     * This map is used to provide data to the SimpleAdapter above. See the
     * fillMap() function for how it relates observers to rows in the displayed
     * activity.
     */
    private ArrayList<HashMap<String, Object>> transferRecordMaps;

    // Which row in the UI is currently checked (if any)
    private int checkedIndex;

//    @BindView(R.id.b1)
//    Button one;
//
//    @OnClick(R.id.b1)
//    public void upload(){
//        Log.e(TAG,"clicked");
//        beginUpload();
////        startActivity(new Intent(LogInActivity.this, UploadActivity.class));
//    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
//        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//        StrictMode.setVmPolicy(builder.build());
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        transferUtility = Util.getTransferUtility(this);
        replaceFragment();
    }

    private void replaceFragment() {
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        LogInFragment fragment = new LogInFragment();
        ft.replace(R.id.fragmentFrame, fragment, LogInFragment.TAG);
        ft.commit();
    }

    @Override
    public void onPasswordReset(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        // Catch back action and pops from backstack
        // (if you called previously to addToBackStack() in your transaction)
        if (getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStack();
        }
        // Default action on back pressed
        else super.onBackPressed();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
