package com.eventime.eventime.screens.events;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eventime.eventime.R;
import com.eventime.eventime.model.Event;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.eventime.eventime.utils.TimeHandler.parseDate;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Event> myEvents;
    private FragmentActivity activity;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvTitle;
        public TextView tvLocation;
        public TextView tvStartDate;
        public ImageView iv_event_image;

        public ViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvLocation = (TextView) view.findViewById(R.id.tv_location);
            tvStartDate = (TextView) view.findViewById(R.id.tv_time);
            iv_event_image = (ImageView) view.findViewById(R.id.iv_event_image);
        }
    }

    public Event getElement(int position){
       return myEvents.get(position);
    }

    public EventAdapter(ArrayList<Event> events, Context mContext, FragmentActivity fa) {
        myEvents = events;
        context = mContext;
        activity = fa;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public EventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View eventView = inflater.inflate(R.layout.item_event, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(eventView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Event thisEvent = myEvents.get(position);
        holder.tvTitle.setText(thisEvent.getName());
        //tvLocation.setText("Manhattan, New York");
//        Log.e("Address", events.get(position).getAddress());
        if(thisEvent.getAddress().length()>27)
            holder.tvLocation.setText(thisEvent.getAddress().substring(0,27)+ "...");
        else {
            holder.tvLocation.setText(thisEvent.getAddress());
        }
        holder.tvStartDate.setText(holder.itemView.getContext().getResources().getString(R.string.start_date_item)+" "+parseDate(thisEvent.getStartTime()));
        if(thisEvent.getImage()!=null){
            if(thisEvent.getImage()==null){
                ResourcesCompat.getDrawable(holder.itemView.getContext().getResources(), R.mipmap.ic_launcher, null);
            }else{
                Picasso.with(EventAdapter.this.context).load(thisEvent.getImage()).into(holder.iv_event_image);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(0, thisEvent, position);
            }
        });

    }
    private void replaceFragment(int code, Event event, int position) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        Bundle b = new Bundle();
        b.putParcelable("event",event);
        b.putInt("position",position);

        ft.addToBackStack("EventsFragment");
//ODKOMENTOWAC
//        if(code==0){
//            EventViewFragment fragment = new EventViewFragment();
//            fragment.setArguments(b);
//            ft.replace(R.id.fragmentFrame, fragment, EventViewFragment.TAG);
//        }
        ft.commit();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return myEvents.size();
    }
}
