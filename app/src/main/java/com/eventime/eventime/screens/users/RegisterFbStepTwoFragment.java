package com.eventime.eventime.screens.users;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.eventime.eventime.R;
import com.eventime.eventime.model.User;
import com.eventime.eventime.utils.Constants;
import com.eventime.eventime.utils.PictureInfo;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFbStepTwoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFbStepTwoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFbStepTwoFragment extends Fragment {

    public static final String TAG = RegisterFbStepTwoFragment.class.getSimpleName();

    private RegisterFbStepTwoFragment.OnFragmentInteractionListener mListener;

    User user;
    PictureInfo pcInfo;
    private ProgressDialog progress;

    ArrayAdapter<CharSequence> adapterAffilation;
    ArrayAdapter<CharSequence> adapterRace;

    // With underscore - a button, without - a field
    @BindView(R.id.b_next)
    Button bNext;
    @BindView(R.id.s_politicalAff)
    Spinner sPoliticalAff;
    @BindView(R.id.s_race)
    Spinner sRace;
    @BindView(R.id.et_location)
    EditText etLocation;

    @OnClick(R.id.b_next)
    public void continueButtonPressed(){

        String politicalAff;
        String location;
        String race;
        pcInfo.setUserPicture(UUID.randomUUID().toString() + ".png");

        user.setPicture(Constants.USER_PICTURE_BUCKET+pcInfo.getUserPicture());
        user.setFacebook(true);

        replaceFragment(2);// go to Preferences
    }

    public RegisterFbStepTwoFragment() {
        // Required empty public constructor
    }


    public static RegisterFbStepTwoFragment newInstance(String param1, String param2) {
        RegisterFbStepTwoFragment fragment = new RegisterFbStepTwoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_fb_step_two, container, false);
        ButterKnife.bind(this, view);
        user = getArguments().getParcelable("userData");

        adapterAffilation = ArrayAdapter.createFromResource(getActivity(), R.array.affilations, android.R.layout.simple_spinner_item);
        adapterAffilation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sPoliticalAff.setAdapter(adapterAffilation);
        sPoliticalAff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //userGender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterRace = ArrayAdapter.createFromResource(getActivity(), R.array.race, android.R.layout.simple_spinner_item);
        adapterRace.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sRace.setAdapter(adapterRace);
        sRace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //userGender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void replaceFragment(int code) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("userData",user);
        bundle.putParcelable("pcInfo", pcInfo);
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        if(code==0){
            RegisterStepOneFragment fragment = new RegisterStepOneFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, RegisterStepOneFragment.TAG);
        }
        else if(code==1){
            RegisterStepTwoFragment fragment = new RegisterStepTwoFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, RegisterStepTwoFragment.TAG);
        }
        else {
            RegisterStepThreeFragment fragment = new RegisterStepThreeFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, RegisterStepThreeFragment.TAG);
        }

        ft.commit();

    }
}
