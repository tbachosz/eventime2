package com.eventime.eventime.screens.events;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.eventime.eventime.R;
import com.eventime.eventime.screens.MainActivity;
import com.eventime.eventime.model.MyLocationLatLon;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.utils.Constants;
import com.eventime.eventime.utils.ImagesHandler;
import com.eventime.eventime.utils.PermissionUtils;
import com.eventime.eventime.utils.PictureInfo;
import com.eventime.eventime.utils.Util;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.eventime.eventime.screens.events.CreateNewEventActivity.myEvent;
import static com.eventime.eventime.utils.Constants.LOCATION_REQUEST;
import static com.eventime.eventime.utils.Constants.loggedUser;

public class NewEventStepFourFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, LocationListener {

    public static final String TAG = NewEventStepFourFragment.class.getSimpleName();

    // The TransferUtility is the primary class for managing transfer to S3
    private TransferUtility transferUtility;
    private ProgressDialog progress;
    public PictureInfo pcInfo;

    private OnFragmentInteractionListener mListener;

    private CompositeSubscription mSubscriptions;

//    private Event myEvent;

    private MapView mapView;
    private GoogleMap googleMap;
    private Marker marker;
    private String currentAddress;

    private LocationManager locationManager;

    @BindView(R.id.b_create_event)
    Button bCreateEvent;
    @BindView(R.id.tv_map_ref)
    TextView tvMapRef;
    @BindView(R.id.sv_location)
    SearchView searchLocation;

    @OnClick(R.id.b_create_event)
    public void onLocSaveClick(){
        createEventProcess();
        //On success replaces fragment
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    private void createEventProcess() {
        if(marker==null){
            Toast.makeText(getActivity(), "Select the location!", Toast.LENGTH_LONG).show();
            return;
        }
        myEvent.setAddress(currentAddress);
//        Event event = new Event("name","host","description","type","topic", null, currentAddress, new MyLocationLatLon(marker.getPosition().longitude, marker.getPosition().latitude),123);
        myEvent.setLocation(new MyLocationLatLon(marker.getPosition().longitude, marker.getPosition().latitude));
        Log.e("THIS EVENT IS",myEvent.toString());
        if(pcInfo!= null)
            if(pcInfo.getUserPicture() != null)
                myEvent.setImage(pcInfo.getUserPicture());

        progress.show();
        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).createEvent(loggedUser.getEmail(), myEvent) // these info get from user
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }

    private void handleResponse(Response response) {
        if(response.getToken() != null)
            Constants.saveAccessToken(getActivity(), response.getToken());

        Log.e(TAG, "Created!: " + response.toString());
        if(myEvent.getImage()!=null) {
            beginUpload();
        }
        else{
            Log.e(TAG, getString(R.string.default_pic_log));
            progress.dismiss();
            startActivity(new Intent(getActivity(), MainActivity.class));
        }

    }

    private void handleError(Throwable error) {
        progress.dismiss();

        Log.e(TAG, "Create error!: " + error.getMessage());

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    public NewEventStepFourFragment() {
        // Required empty public constructor
    }

    public static NewEventStepFourFragment newInstance(String param1, String param2) {
        NewEventStepFourFragment fragment = new NewEventStepFourFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        transferUtility = Util.getTransferUtility(getActivity());
        progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(R.string.creating_event));
        progress.setCancelable(false);
        Log.e("MY EVENT NOW",myEvent.toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_event_step_four, container, false);
        ButterKnife.bind(this,view);
//        myEvent = getArguments().getParcelable("event");
        pcInfo = getArguments().getParcelable("pcInfo");

        locationPermission(); //checks if permission for location is already turned on
        initSearchView();

        return view;
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

//    private void replaceFragment(int code) {
//        Bundle bundle = new Bundle();
//        myEvent.setLocation(new MyLocationLatLon(marker.getPosition().longitude, marker.getPosition().latitude));
//        myEvent.setAddress(currentAddress);
//        bundle.putParcelable("event",myEvent); //CIPEK PUT EVENT HERE NOW WITH ADDED LOCATION
//        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//        if(code==0){
//            RegisterStepOneFragment fragment = new RegisterStepOneFragment();
//            fragment.setArguments(bundle);
////            ft.replace(R.id.fragmentFrame, fragment, RegisterStepOneFragment.TAG);
//        }
//        ft.commit();
//    }

//    private void beginUpload(){
//        Log.e(TAG,CreateNewEventActivity.picUri.toString());
//        Log.e(TAG,CreateNewEventActivity.picName);
//        if(CreateNewEventActivity.picBitmap!=null)
//            Log.e("Bit","FULL");
//        else
//            Log.e("Bit","NULL");
////        Uri myUri = Uri.parse(pcInfo.getPictureUri()+pcInfo.getUserPicture());
//        TransferObserver observer = transferUtility.upload(Constants.BUCKET_EVENT, CreateNewEventActivity.picName,
//                storeImage(CreateNewEventActivity.picBitmap, CreateNewEventActivity.picName));
//    }


    private void beginUpload(){
        TransferObserver observer = ImagesHandler.beginUpload(getActivity(), Constants.BUCKET_EVENT, CreateNewEventActivity.picBitmap, CreateNewEventActivity.picName);

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    Log.d(TAG, "Upload Completed");
                    progress.dismiss();
                    startActivity(new Intent(getActivity(), MainActivity.class));
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {}

            @Override
            public void onError(int id, Exception ex) {}
        });

    }

    private void locationPermission(){
        if (PermissionUtils.requestPermissionFragment(getActivity(), this, LOCATION_REQUEST, Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "onPermissionsResult");
//        switch (requestCode) {
//            case LOCATION_REQUEST:
//                if (PermissionUtils.permissionGranted(requestCode, LOCATION_REQUEST, grantResults)) {
//                    findMyLocation();
//                }
//                else{
//                    setDefaultLocation();
//                }
//                break;
//        }
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
//            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
//                    Manifest.permission.ACCESS_FINE_LOCATION, true);
            gotoLocation(40.712783699999996,-74.0059413, 13);
        } else if (googleMap != null) {
            // Access to the location has been granted to the app.
            googleMap.setMyLocationEnabled(true);
            locationManager = (LocationManager)
                    getActivity().getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            if (location != null && location.getTime() > Calendar.getInstance().getTimeInMillis() - 2 * 60 * 1000) {
                // Do something with the recent location fix
                //  otherwise wait for the update below
                gotoLocation(location.getLatitude(), location.getLongitude(), 13);
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }

        }
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
//            Log.e("Location Changed", location.getLatitude() + " and " + location.getLongitude());
            if(marker==null) {
                gotoLocation(location.getLatitude(), location.getLongitude(), 13);
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            locationManager.removeUpdates(this);
        }
    }

    // Required functions
    public void onProviderDisabled(String arg0) {}
    public void onProviderEnabled(String arg0) {}
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}


    private void gotoLocation(double lat, double lng, float zoom) {
        LatLng latLng = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        googleMap.moveCamera(update);

        Geocoder gc = new Geocoder(getActivity());
        List<Address> list = null;

        try {
            list = gc.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Address add = list.get(0);

        tvMapRef.setText(add.getAddressLine(0));
        addMarker(add, lat, lng);
    }


    @Override
    public final void onStart (){
        super.onStart();
    }

    @Override
    public final void onResume(){
        super.onResume();
    }

    @Override
    public final void onPause(){
        super.onPause();
    }

    @Override
    public final void onStop(){
        super.onStop();
    }

    @Override
    public final void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public final void onLowMemory(){
        super.onLowMemory();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        googleMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();
        initMap();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private void initMap() {

        if (googleMap != null) {
            googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    return null;
                }
            });

            googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    Geocoder gc = new Geocoder(getActivity());
                    List<Address> list = null;

                    try {
                        list = gc.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }

                    Address add = list.get(0);
                    tvMapRef.setText(add.getAddressLine(0));
                    addMarker(add, latLng.latitude, latLng.longitude);
                }
            });

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String msg = marker.getTitle() + " (" +
                            marker.getPosition().latitude + ", " +
                            marker.getPosition().longitude + ")";
                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {
                }

                @Override
                public void onMarkerDrag(Marker marker) {
                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    Geocoder gc = new Geocoder(getActivity());
                    List<Address> list = null;
                    LatLng ll = marker.getPosition();
                    try {
                        list = gc.getFromLocation(ll.latitude, ll.longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }

                    Address add = list.get(0);
                    marker.setTitle(add.getLocality());
                    marker.setSnippet(add.getCountryName());
                    marker.showInfoWindow();
                }
            });

        }

    }

    private void addMarker(Address add, double ltt, double lng) {

        if (marker != null) {
            removeEverything();
        }

        LatLng latLng = new LatLng(ltt, lng);
        MarkerOptions options = new MarkerOptions()
                .title(add.getLocality())
                .position(latLng)
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker());
//              .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker));
        String country = add.getCountryName();
        if (country.length() > 0) {
            options.snippet(country);
        }

        marker = googleMap.addMarker(options);
        currentAddress = add.getAddressLine(0);

//        lon = lng;
//        lat = ltt;

    }

    private void removeEverything() {
        marker.remove();
        marker = null;
    }


    private void initSearchView(){
        searchLocation.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            }
        });

        searchLocation.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.v("SearchedLocation",searchLocation.getQuery().toString());
                try {
                    geoLocate(searchLocation.getQuery().toString());
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), R.string.no_location, Toast.LENGTH_LONG).show();
//                    searchLocation.setQuery("", false);
                    tvMapRef.setText(R.string.no_location);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void geoLocate(String searchString) throws IOException {
        Geocoder gc = new Geocoder(getActivity());
        List<Address> list = gc.getFromLocationName(searchString, 1);

        if (list.size() > 0) {
            Address add = list.get(0);

            tvMapRef.setText(add.getAddressLine(0));
            searchLocation.setQuery("", false);

            double lat = add.getLatitude();
            double lng = add.getLongitude();
            gotoLocation(lat, lng, 15);
            addMarker(add, lat, lng);
        }
        else
            throw new IOException("Location doesn't exist");

    }

}
