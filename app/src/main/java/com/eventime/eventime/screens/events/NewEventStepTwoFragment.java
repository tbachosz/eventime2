package com.eventime.eventime.screens.events;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.eventime.eventime.R;
import com.eventime.eventime.utils.PictureInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import rx.subscriptions.CompositeSubscription;

import static com.eventime.eventime.screens.events.CreateNewEventActivity.myEvent;

public class NewEventStepTwoFragment extends Fragment {
    public static final String TAG = NewEventStepTwoFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;

    private CompositeSubscription mSubscriptions;

    public PictureInfo pcInfo;

    private ArrayAdapter<CharSequence> adapterType;

    static TextView tvStartDate;
    @BindView(R.id.b_next)
    Button bNext;
    @BindView(R.id.s_type)
    Spinner spType;
    @BindView(R.id.b_pick_start_date)
    Button bPickStartDate;
    @BindView(R.id.et_description)
    EditText etDescription;

    static String startDate;

    @OnFocusChange(R.id.b_pick_start_date)
    public void onStartDateFocus(){
        if(tvStartDate.isFocused()) {
            DialogFragment newFragment = new DatePickerFragment();
            newFragment.show(getActivity().getFragmentManager(), "datePicker");
        }
    }

    @OnClick(R.id.b_pick_start_date)
    public void onStartDateClick() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getFragmentManager(), "datePicker");
    }

    @OnClick(R.id.b_next)
    public void onContinueClick(){
        if(tvStartDate.getText().equals("")){
            Toast.makeText(getActivity(),getString(R.string.toast_start_date), Toast.LENGTH_LONG).show();
        } else if(etDescription.getText().toString().equals(null)){
            Toast.makeText(getActivity(),getString(R.string.toast_description_event), Toast.LENGTH_LONG).show();
        }
        else {
            String pattern = "MM/dd/yyyy HH:mm";
            SimpleDateFormat format = new SimpleDateFormat(pattern);
            long time = 0;
            Log.e("Time:", "Before");
            if (startDate != null && !startDate.equals("")) {
                try {
                    Date date = format.parse(startDate);
                    time = date.getTime();
                    Log.e("Time:", time + "");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            myEvent.setStartTime(time);
            myEvent.setDescription(etDescription.getText().toString());

            replaceFragment(0);
        }
    }

    public NewEventStepTwoFragment() {
        // Required empty public constructor
    }

    public static NewEventStepTwoFragment newInstance(String param1, String param2) {
        NewEventStepTwoFragment fragment = new NewEventStepTwoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        Log.e("MY EVENT NOW",myEvent.toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_event_step_two, container, false);
        ButterKnife.bind(this, view);
        tvStartDate = (TextView) view.findViewById(R.id.tv_start_date);
        pcInfo=getArguments().getParcelable("picInfo");
        initSpinners();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void initSpinners(){
        adapterType = ArrayAdapter.createFromResource(getActivity(), R.array.event_types, android.R.layout.simple_spinner_item);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spType.setAdapter(adapterType);
        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //userGender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        adapterTopic = ArrayAdapter.createFromResource(getActivity(), R.array.issues, android.R.layout.simple_spinner_item);
//        adapterTopic.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spTopic.setAdapter(adapterTopic);
//        spTopic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                //userGender = parent.getItemAtPosition(position).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
    }

    private void replaceFragment(int code) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("picInfo",pcInfo);
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        ft.addToBackStack("NewEventTwo");

        if(code==0){
            NewEventStepFourFragment fragment = new NewEventStepFourFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, NewEventStepFourFragment.TAG);
        }
        ft.commit();
    }

            public static class DatePickerFragment extends DialogFragment
                    implements DatePickerDialog.OnDateSetListener {

                @Override
                public Dialog onCreateDialog(Bundle savedInstanceState) {
                    // Use the current date as the default date in the picker
                    final Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);

                    // Create a new instance of DatePickerDialog and return it
                    return new DatePickerDialog(getActivity(), this, year, month, day);
                }

                public void onDateSet(DatePicker view, int year, int month, int day) {
                    // Do something with the date chosen by the user
                    if (day < 10) {
                        if (month < 9) {
                            startDate = "0" + (month + 1) + "/0" + day + "/" + year;
                        } else {
                            startDate = (month + 1) + "/0" + day + "/" + year;
                        }
                    } else {
                        if (month < 9) {
                            startDate = "0" + (month + 1) + "/" + day + "/" + year;
                        } else {
                            startDate = (month + 1) + "/" + day + "/" + year;
                        }
                    }

                    DialogFragment newFragment = new TimePickerFragment();
                    newFragment.show(getActivity().getFragmentManager(), "timePicker");
                }
            }

            public static class TimePickerFragment extends DialogFragment
                    implements TimePickerDialog.OnTimeSetListener {

                @Override
                public Dialog onCreateDialog(Bundle savedInstanceState) {
                    // Use the current time as the default values for the picker
                    final Calendar c = Calendar.getInstance();
                    int hour = c.get(Calendar.HOUR_OF_DAY);
                    int minute = c.get(Calendar.MINUTE);

                    // Create a new instance of TimePickerDialog and return it
                    return new TimePickerDialog(getActivity(), this, hour, minute,
                            DateFormat.is24HourFormat(getActivity()));
                }

                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    // Do something with the time chosen by the user
                    if (hourOfDay < 10) {
                        if (minute < 10) {
                            startDate += " 0" + hourOfDay + ":0" + minute;
                        } else {
                            startDate += " 0" + hourOfDay + ":" + minute;
                        }
                    } else {
                        if (minute < 10) {
                            startDate += " " + hourOfDay + ":0" + minute;
                        } else {
                            startDate += " " + hourOfDay + ":" + minute;
                        }
                    }
                    tvStartDate.setText(startDate);
                }
            }

    }
