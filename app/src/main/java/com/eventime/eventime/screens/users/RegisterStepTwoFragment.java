package com.eventime.eventime.screens.users;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eventime.eventime.R;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.model.User;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.utils.PictureInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class RegisterStepTwoFragment extends Fragment {

    public static final String TAG = RegisterStepTwoFragment.class.getSimpleName();
    private CompositeSubscription mSubscriptions;
    private ProgressDialog progress;

    public static User user;

    public PictureInfo pcInfo;

    // With underscore - a button, without - a field
    @BindView(R.id.b_next)
    Button bNext;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_passwordConfirm)
    EditText etPasswordConfirm;

    @BindView(R.id.ti_email)
    TextInputLayout tiEmail;
    @BindView(R.id.ti_password)
    TextInputLayout tiPassword;
    @BindView(R.id.ti_passwordConfirm)
    TextInputLayout tiPasswordConfirm;

    @OnClick(R.id.b_next)
    public void continueButtonPressed() {

        clearErrors();

        String email;
        String password;
        String password2;

        email = etEmail.getText().toString();
        password = etPassword.getText().toString();
        password2 = etPasswordConfirm.getText().toString();

        if (!email.contains("@"))//czy wykrzyknik jest zaprzeczeniem?
            tiEmail.setError(getResources().getString(R.string.emailNotValid));
        else if (password.length() < 8)
            tiPassword.setError(getResources().getString(R.string.passwordTooShort));
        else if (!password.equals(password2))
            tiPasswordConfirm.setError(getResources().getString(R.string.passwordsDontMatch));
        else {

            user.setEmail(email);
            user.setPassword(password);

            CheckEmailIsAlreadyUsed(email);

        }
    }

    private void clearErrors() {
        tiEmail.setError(null);
        tiPassword.setError(null);
        tiPasswordConfirm.setError(null);
    }

    private void replaceFragment(int code) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("userData", user);
        bundle.putParcelable("pcInfo", pcInfo);
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        ft.addToBackStack("RegisterStepTwo");

//        if (code == 0) {
//            RegisterStepOneFragment fragment = new RegisterStepOneFragment();
//            fragment.setArguments(bundle);
//            ft.replace(R.id.fragmentFrame, fragment, RegisterStepOneFragment.TAG);
         if (code == 1) {
            RegisterStepThreeFragment fragment = new RegisterStepThreeFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, RegisterStepThreeFragment.TAG);
        }
//        else {
//            PreferencesFragment fragment = new PreferencesFragment();
//            fragment.setArguments(bundle);
//            ft.replace(R.id.fragmentFrame, fragment, PreferencesFragment.TAG);
//        }

        ft.commit();

    }

    private RegisterStepTwoFragment.OnFragmentInteractionListener mListener;

    public RegisterStepTwoFragment() {
        // Required empty public constructor
    }

    public static RegisterStepTwoFragment newInstance(String param1, String param2) {
        RegisterStepTwoFragment fragment = new RegisterStepTwoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(R.string.checking_emial));
        progress.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_step_two, container, false);
        ButterKnife.bind(this, view);
        user = getArguments().getParcelable("userData");
        pcInfo = getArguments().getParcelable("pcInfo");
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    public void CheckEmailIsAlreadyUsed(String email) {
        progress.show();
        mSubscriptions.add(NetworkUtil.getRetrofit().checkEmail(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }

    private void handleResponse(Response response) {
        Log.e(TAG, "Login succeeded!: " + response.toString());
        progress.dismiss();
        if (response.getIsAvailable())
            replaceFragment(1);
        else {
            tiEmail.setError(getString(R.string.email_in_use));
            showSnackBarMessage(getString(R.string.email_in_use));
        }
    }


    private void handleError(Throwable error) {
        Log.e(TAG, "Email error!: " + error.getMessage());

//        mProgressBar.setVisibility(View.GONE);
        progress.dismiss();

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            showSnackBarMessage("Network Error !");
        }
    }

    private void showSnackBarMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }
}
