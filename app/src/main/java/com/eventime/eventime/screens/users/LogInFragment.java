package com.eventime.eventime.screens.users;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eventime.eventime.R;
import com.eventime.eventime.screens.MainActivity;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.model.User;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.utils.Constants;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.eventime.eventime.utils.Validation.validateEmail;
import static com.eventime.eventime.utils.Validation.validateFields;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LogInFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LogInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogInFragment extends Fragment {
    public static final String TAG = LogInFragment.class.getSimpleName();

    public String FCMtoken;

    private CompositeSubscription mSubscriptions;
    private SharedPreferences mSharedPreferences;
    private ProgressDialog progress;
    //Fb Login
    public User user;
    private AccessTokenTracker mTokenTracker;
    private ProfileTracker mProfileTracker;
    CallbackManager callbackManager;
    public static Profile profile;
    String username, email, gender, birthday, picture=null;//is Uri

    Animation leftToRight;
    Animation rightToLeft;

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.e(TAG, "login successful");
//            getString(R.string.logging_in)
//            progress = ProgressDialog.show(getActivity(), "dialog title",
//                    getString(R.string.logging_in), true);
            progress.show();

            if(Profile.getCurrentProfile() == null) {
                mProfileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                        Log.v("facebook - profile", currentProfile.getFirstName());
                        mProfileTracker.stopTracking();
                        getFbDetails(loginResult);
                    }
                };
                // no need to call startTracking() on mProfileTracker
                // because it is called by its constructor, internally.
            }
            else {
                profile = Profile.getCurrentProfile();
                Log.v("facebook - profile", profile.getFirstName());
                getFbDetails(loginResult);
            }

//            Log.e("LoginResult", loginResult.toString());
//            Log.e("ProfileFB", Profile.getCurrentProfile().toString());
//            profile = Profile.getCurrentProfile();
//            profile.getLinkUri();

            Toast.makeText(getActivity(), "login successful", Toast.LENGTH_LONG).show();

        }

        @Override
        public void onCancel() {
            Log.e(TAG, "login cancelled");
            Toast.makeText(getActivity(), "login cancelled", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(FacebookException error) {
            Log.e(TAG, "login error");
            Toast.makeText(getActivity(), "login error", Toast.LENGTH_LONG).show();
        }
    };

//    @BindView(R.id.fb_login_button)
//    LoginButton fbLogin;
    @BindView(R.id.b_log_in)
    Button loginButton;
    @BindView(R.id.ti_email)
    TextInputLayout tiEmail;
    @BindView(R.id.ti_password)
    TextInputLayout tiPassword;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.progress)
    ProgressBar mProgressBar;
    @BindView(R.id.tv_Register)
    TextView tvRegister;

    @OnClick(R.id.b_log_in)
    public void onLogInClick(){
//        loginButton.setAnimation(
//                AnimationUtils.loadAnimation(getActivity(),R.animator.flip_right_in)
//   );
//        AnimationUtils.loadAnimation(getActivity(),R.anim.flip_right_out),
//                AnimationUtils.loadAnimation(getActivity(),R.anim.flip_left_out),
//                AnimationUtils.loadAnimation(getActivity(),R.anim.flip_left_in)

        login();
    }

    public void CheckIfUserRegistered(String email){
        //is_registered
        FCMtoken = "temp";//FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "FCM token: " + FCMtoken);
        mSubscriptions.add(NetworkUtil.getRetrofit().checkIfUserRegistered(email, new User(FCMtoken))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseFb,this::handleError));
    }

    private OnFragmentInteractionListener mListener;

    public LogInFragment() {
        // Required empty public constructor
    }

    public static LogInFragment newInstance(String param1, String param2) {
        LogInFragment fragment = new LogInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        FirebaseApp.initializeApp(getActivity());
        user = new User();
        callbackManager = CallbackManager.Factory.create();
        initSharedPreferences();
        mSubscriptions = new CompositeSubscription();
        progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(R.string.logging_in));
        progress.setCancelable(false);

        setupTokenTracker();
        setupProfileTracker();
        mTokenTracker.startTracking();
        mProfileTracker.startTracking();
        checkLoggedIn();

        if(isLoggedIn()){
            Log.e(TAG,"User is logged in");
            progress.dismiss();
            startActivity(new Intent(getActivity(), MainActivity.class));
            Log.e(TAG,"User is logged in2");
        }
        //loginButton.setOnClickListener(view -> login());
        //initViews(view);
    }


//    private void setupFbLoginButton(View view) {
//        fbLogin.setFragment(this);
//        fbLogin.setReadPermissions("user_friends", "email", "user_birthday", "picture");
//        fbLogin.registerCallback(callbackManager, mFacebookCallback);
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log_in, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        InitilalizeSpans();
//        setupFbLoginButton(view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setupTokenTracker() {
        mTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Log.d(TAG, "" + currentAccessToken);
            }
        };
    }

    private void setupProfileTracker() {
        mProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                Log.d(TAG, "" + currentProfile);
//                mTextDetails.setText(constructWelcomeMessage(currentProfile));
            }
        };
    }

    public void goToRegisterUser() {
        replaceFragment(0);
    }

    public void forgotPassword() {
        ResetPasswordDialog fragment = new ResetPasswordDialog();

        fragment.show(getActivity().getSupportFragmentManager().beginTransaction(), ResetPasswordDialog.TAG);
    }

    private void initSharedPreferences() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    private void login() {

        setError();

        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        int err = 0;

        if (!validateEmail(email)) {

            err++;
            tiEmail.setError("Email should be valid !");
        }

        if (!validateFields(password)) {

            err++;
            tiPassword.setError("Password should not be empty !");
        }

        if (err == 0) {
            //If no error occurrs, proceed with login
            loginProcess(email, password);
            progress.show();
//            progress = ProgressDialog.show(getActivity(), "dialog title",
//                    getString(R.string.logging_in), true);
//            mProgressBar.setVisibility(View.VISIBLE);

        } else {

            showSnackBarMessage("Enter Valid Details !");
        }
    }

    private void setError() {
        tiEmail.setError(null);
        tiPassword.setError(null);
    }

    private void loginProcess(String email, String password) {
//        FCMtoken = FirebaseInstanceId.getInstance().getToken();
//        Log.d(TAG, "FCM token: " + FCMtoken);
        FCMtoken = "temp";
        mSubscriptions.add(NetworkUtil.getRetrofit(email, password).login(new User(FCMtoken))//Adding a dummy user to send FCM (otherwise, we cannot send strings)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseLogin, this::handleError));
    }

    private void handleResponseFb(Response response) {

        Log.e(TAG,"User did not finish registration");
        if(response.getIsRegistered()==false){
            progress.dismiss();
            user.setName(username);
            user.setEmail(email);
            user.setGender(gender);
            user.setBirthday(birthday);
            //user.setPicture(picture);
            replaceFragment(1);
        }
        else{
            //Move to events
            Toast.makeText(getActivity(), "Logged ok",Toast.LENGTH_LONG).show();
            //loadProfile(response.getToken(),response.getRefreshToken(),response.getMessage());
//            startActivity(new Intent(getActivity(), MainActivity.class));
            Constants.saveTokens(getActivity(), response.getToken(), response.getRefreshToken(), email);

            loadProfile(response.getToken(),response.getRefreshToken(),email);

        }

    }

    private void handleResponseLogin(Response response) {
        Log.e(TAG, "Login succeeded!: " + response.toString());

//        SharedPreferences.Editor editor = mSharedPreferences.edit();
//        editor.putString(Constants.TOKEN, response.getToken());
//        editor.putString(Constants.EMAIL, response.getMessage());
//        editor.putString(Constants.REFRESH_TOKEN, response.getRefreshToken());
//        editor.apply();

        Constants.saveTokens(getActivity(), response.getToken(), response.getRefreshToken(), response.getMessage());

        loadProfile(response.getToken(),response.getRefreshToken(),response.getMessage());
    }

    private void handleError(Throwable error) {

        Log.e(TAG, "Login error!: " + error.getMessage());

//        mProgressBar.setVisibility(View.GONE);
        progress.dismiss();

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            showSnackBarMessage("Network Error !");
        }
    }


    private void loadProfile(String mToken, String mRefreshToken, String mEmail) {

        Log.e("RELOGIN", "token: " + mToken + ", email: " + mEmail + ", refreshToken: " + mRefreshToken);

        mSubscriptions.add(NetworkUtil.getRetrofit(mToken, mRefreshToken, Constants.getEmail(getActivity())).getProfile(mEmail)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseProfile,this::handleError));
    }

    private void handleResponseProfile(User user) {
        Constants.loggedUser = user;

        if(user.getToken()!=null){
//            SharedPreferences.Editor editor = mSharedPreferences.edit();
//            editor.putString(Constants.TOKEN, user.getToken());
//            editor.apply();

            Constants.saveAccessToken(getActivity(), user.getToken());
        }

//        mProgressBar.setVisibility(View.GONE);

        etEmail.setText(null);
        etPassword.setText(null);
        progress.dismiss();

        startActivity(new Intent(getActivity(), MainActivity.class));
    }

    private void showSnackBarMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    private void checkLoggedIn() {
//        String tempToken = mSharedPreferences.getString(Constants.TOKEN, "");
//        String tempEmail = mSharedPreferences.getString(Constants.EMAIL, "");
//        String tempRefreshToken = mSharedPreferences.getString(Constants.REFRESH_TOKEN, "");
        if (!Constants.getAccessToken(getActivity()).equals("") && !Constants.getEmail(getActivity()).equals("") && !Constants.getRefreshToken(getActivity()).equals("")) {
            //mProgressBar.setVisibility(View.VISIBLE);
            progress.show();
            Log.e("REFRESH TOKEN relogin", Constants.getEmail(getActivity()));
            ReLoginProcess(Constants.getAccessToken(getActivity()), Constants.getEmail(getActivity()), Constants.getRefreshToken(getActivity()));
        }
    }

    private void ReLoginProcess(String token, String email, String refreshToken) {

        Log.e("RELOGIN", "token: " + token + ", email: " + email + ", refreshToken: " + refreshToken);

        mSubscriptions.add(NetworkUtil.getRetrofit(token, refreshToken, Constants.getEmail(getActivity())).reLogin()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseReLogin, this::handleErrorReLogin));
    }

    private void handleResponseReLogin(Response response) {

        //mProgressBar.setVisibility(View.GONE);

        if (response.getToken() != null) {
//            SharedPreferences.Editor editor = mSharedPreferences.edit();
//            editor.putString(Constants.TOKEN, response.getToken());
//            editor.apply();
            Constants.saveAccessToken(getActivity(), response.getToken());
        }

        etEmail.setText(null);
        etPassword.setText(null);

//
//        String mToken = mSharedPreferences.getString(Constants.TOKEN,"");
//        String mRefreshToken = mSharedPreferences.getString(Constants.REFRESH_TOKEN,"");
//        String mEmail = mSharedPreferences.getString(Constants.EMAIL,"");

        loadProfile(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity()));

    }

    private void handleErrorReLogin(Throwable error) {

        //mProgressBar.setVisibility(View.GONE);
        progress.dismiss();

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            showSnackBarMessage("Network Error !");
        }
    }

    public void getFbDetails(LoginResult loginResult){
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Log.v("LogInActivity Response ", response.toString());

                        try {
                            Log.e("KEYS", " " + object.names().toString());
                            username = object.getString("name");
                            email = object.getString("email");
                            gender = object.getString("gender");
                            birthday = object.getString("birthday");
//                            if(object.getString("picture")!=null){
//                                picture=object.getString("picture");
//                            };


                            Log.v("FbEmail = ", " " + username);
                            Log.v("FbEmail = ", " " + email);
                            Log.v("FbEmail = ", " " + gender);
                            Log.e("FbGender", " " + birthday);
//                            Log.v("FbImgUri",""+ picture);

                            CheckIfUserRegistered(email);

                            //setting user data

                            //getFreinds(object.getString("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void replaceFragment(int code) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("userData",user);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        ft.addToBackStack("LogInFragment");

        if(code==0){
            RegisterStepOneFragment fragment = new RegisterStepOneFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, RegisterStepOneFragment.TAG);
        }
        else if(code==1){
            RegisterFbStepTwoFragment fragment = new RegisterFbStepTwoFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, RegisterFbStepTwoFragment.TAG);
        }

        ft.commit();

    }

    public void InitilalizeSpans() {
        String terms = getString(R.string.register);
        ClickableSpan click = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Log.e(TAG, "register clicked");
                goToRegisterUser();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan click2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Log.e(TAG, "register clicked");
                forgotPassword();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(terms);
        spannableStringBuilder.setSpan(click, 15, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE); //"register" word
        spannableStringBuilder.setSpan(click2, 36, terms.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvRegister.setText(spannableStringBuilder);
        tvRegister.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onResume() {
        super.onResume();
        profile = Profile.getCurrentProfile();
//        mTextDetails.setText(constructWelcomeMessage(profile));
    }

    @Override
    public void onStop() {
        super.onStop();
        mTokenTracker.stopTracking();
        mProfileTracker.stopTracking();
    }

}
