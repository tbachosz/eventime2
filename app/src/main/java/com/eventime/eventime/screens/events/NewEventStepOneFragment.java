package com.eventime.eventime.screens.events;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.eventime.eventime.R;
import com.eventime.eventime.utils.Constants;
import com.eventime.eventime.utils.PictureDialog;
import com.eventime.eventime.utils.PictureInfo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subscriptions.CompositeSubscription;

import static com.eventime.eventime.screens.events.CreateNewEventActivity.myEvent;
import static com.eventime.eventime.utils.Constants.loggedUser;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewEventStepOneFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewEventStepOneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewEventStepOneFragment extends Fragment {
    public static final String TAG = NewEventStepOneFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;

    private CompositeSubscription mSubscriptions;

//    public static Event myEvent = new Event();

    ArrayAdapter<CharSequence> adapterHost;
    ArrayList<String> hosts = new ArrayList<String>();

    public static ImageButton ibEventImage;
    public static String pictureName = null;
    public static Uri pictureUri = null;
    public PictureInfo pcInfo;

    @BindView(R.id.et_name)
    EditText etName;
//    @BindView(R.id.s_host)
//    Spinner sHost;
    @BindView(R.id.b_event_continue)
    Button bContinue;

    @OnClick(R.id.b_event_continue)
    public void onContinueClick(){
//        myEvent.setHost(loggedUser.getEmail());
        if(etName.getText().toString().equals(null)){
            Toast.makeText(getActivity(),getString(R.string.toast_name_event), Toast.LENGTH_LONG).show();
        }else{
            myEvent.setName(etName.getText().toString());
        }

//        setHost();

        if(CreateNewEventActivity.picUri!=null){
            Log.v(TAG,"pictureURI"+pictureUri);
            pictureUri=CreateNewEventActivity.picUri;
            pictureName=CreateNewEventActivity.picName;
            pcInfo = new PictureInfo(pictureUri.toString(), pictureName);
            myEvent.setImage(Constants.EVENT_PICTURE_BUCKET + pictureName);
        }else{
            Log.v(TAG, "Uri is null");
            pcInfo = new PictureInfo(null, null);
            myEvent.setImage(null);
        }
        Log.e("MY EVENT NOW",myEvent.toString());

        replaceFragment(0);
    }

//    public void setHost(){
////        int result = loggedUser.whoIsHosting(sHost.getSelectedItem().toString());
////        if(result==1){
//            myEvent.setHost(loggedUser.getEmail());
////        }else if(result==2){
////            myEvent.setCreatedByOrganization(loggedUser.getOrganizationByName(sHost.getSelectedItem().toString()).getId());
////        }
//
//    }

    @OnClick(R.id.ib_event_image)
    public void openPictureDialog(){
        PictureDialog fragment = new PictureDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("caller",Constants.Caller.NEW_EVENT);//if caller is true dialog comes from registration, if false then from newEvent
        fragment.setArguments(bundle);
        fragment.show(getActivity().getSupportFragmentManager().beginTransaction(), PictureDialog.TAG);
    }

    public NewEventStepOneFragment() {
        // Required empty public constructor
    }

    public static NewEventStepOneFragment newInstance(String param1, String param2) {
        NewEventStepOneFragment fragment = new NewEventStepOneFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        if(savedInstanceState != null) {
            Bitmap bitmap = savedInstanceState.getParcelable("image");
            ibEventImage.setImageBitmap(bitmap);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_event_step_one, container, false);
        ButterKnife.bind(this, view);
        ibEventImage = (ImageButton)view.findViewById(R.id.ib_event_image);
        Log.e("MY EVENT NOW",myEvent.toString());

        return view;
    }
//Solution: dodac pole names w modelu Usera i to bedzie array string ktory posaida wszystkei organizacje i usera. Potem to sie wyswietla w spinnerze i nastepnie szuka odpowiedniego obiektu
    //po wlasciwosciach jakie ma user.

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

   public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void replaceFragment(int code) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("event",myEvent);
        bundle.putParcelable("picInfo",pcInfo);
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        ft.addToBackStack("RegisterStepOne");

        if(code==0){
            NewEventStepTwoFragment fragment = new NewEventStepTwoFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, NewEventStepTwoFragment.TAG);
        }
        ft.commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
//        Drawable drawable = ibEventImage.getBackground();
//        Bitmap bitmap = drawableToBitmap(drawable);
//        outState.putParcelable("image", bitmap);
//        super.onSaveInstanceState(outState);
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

}
