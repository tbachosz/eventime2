package com.eventime.eventime.screens.users;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.eventime.eventime.R;
import com.eventime.eventime.model.User;
import com.eventime.eventime.utils.PictureInfo;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.utils.Constants;
import com.eventime.eventime.utils.ImagesHandler;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class RegisterStepThreeFragment extends Fragment {

    public static final String TAG = RegisterStepTwoFragment.class.getSimpleName();

    User user;

    private CompositeSubscription mSubscriptions;
    private ProgressDialog progress;
    TransferObserver observer;

    ArrayAdapter<CharSequence> adapterGender;

    private OnFragmentInteractionListener mListener;

    static EditText etBirthday;

    private PictureInfo pcInfo;

    private LocationManager locationManager;
    private LatLng latLng = null;

    // With underscore - a button, without - a field
    @BindView(R.id.b_next)
    Button bNext;

    @BindView(R.id.s_gender)
    Spinner sGender;
    @BindView(R.id.b_birthday)
    Button bBirthday;

    public static TextView tvBirthdayDisplay;

    @OnClick(R.id.b_next)
    public void continueButtonPressed() {

        user.setBirthday(tvBirthdayDisplay.getText().toString());
        user.setGender(sGender.getSelectedItem().toString());
        user.setFacebook(false);

        if(user.getFacebook()==true){
            registerProcessFb(user);
        }
        else{
            registerProcess(user);
        }
    }

    @OnClick(R.id.b_birthday)
    public void showTruitonDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getFragmentManager(), "datePicker");//was getSupportFragmentManager
    }

    public RegisterStepThreeFragment() {
        // Required empty public constructor
    }

    public static RegisterStepThreeFragment newInstance(String param1, String param2) {
        RegisterStepThreeFragment fragment = new RegisterStepThreeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_step_three, container, false);
        ButterKnife.bind(this, view);
        progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(R.string.getting_coordinates));

        user = getArguments().getParcelable("userData");
        pcInfo = getArguments().getParcelable("pcInfo");

        tvBirthdayDisplay = (TextView) view.findViewById(R.id.tv_birthday);

        adapterGender = ArrayAdapter.createFromResource(getActivity(), R.array.genders, android.R.layout.simple_spinner_item);
        adapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sGender.setAdapter(adapterGender);
        sGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //userGender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Date d = new Date();

        String pattern = "MM/dd/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(pattern);

        tvBirthdayDisplay.setText(format.format(d));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            Date today = new Date();
            c.setTime(today);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            c.add(Calendar.MONTH, -156);
            long maxDate = c.getTime().getTime();
            dialog.getDatePicker().setMaxDate(maxDate); //Trying to set the minimum to be at least 13 years old from today
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            if (day < 10) {
                if (month < 9) {
                    RegisterStepThreeFragment.tvBirthdayDisplay.setText("0" + (month + 1) + "/0" + day + "/" + year);
                } else {
                    RegisterStepThreeFragment.tvBirthdayDisplay.setText((month + 1) + "/0" + day + "/" + year);
                }
            } else if (month < 9) {
                RegisterStepThreeFragment.tvBirthdayDisplay.setText("0" + (month + 1) + "/" + day + "/" + year);
            } else {
                RegisterStepThreeFragment.tvBirthdayDisplay.setText((month + 1) + "/" + day + "/" + year);
            }

        }

    }
//
//    @Override
//    public final void onStart (){
//        super.onStart();
//    }
//
//    @Override
//    public final void onResume(){
//        super.onResume();
//    }
//
//    @Override
//    public final void onPause(){
//        super.onPause();
//    }
//
//    @Override
//    public final void onStop(){
//        super.onStop();
//    }
//
//    @Override
//    public final void onDestroy(){
//        super.onDestroy();
//    }
//
//    @Override
//    public final void onSaveInstanceState(Bundle savedInstanceState){
//        super.onSaveInstanceState(savedInstanceState);
//    }
//
//    @Override
//    public final void onLowMemory(){
//        super.onLowMemory();
//    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void registerProcess(User user) {
        progress.show();
        mSubscriptions.add(NetworkUtil.getRetrofit().register(user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::handleError));

    }

    private void registerProcessFb(User user) {
        progress.show();
        mSubscriptions.add(NetworkUtil.getRetrofit().registerFb(user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseFB,this::handleError));
    }

    private void handleResponse(Response response) {
        //mProgressbar.setVisibility(View.GONE);
        Log.e("handleResponse", "in");
        if(user.getPicture() != null){
            if(user.getFacebook()==false){
                beginUpload(false);
            }else{
                beginUpload(true);
//                new GraphRequest(
//                        AccessToken.getCurrentAccessToken(),
//                        "/"+LogInFragment.profile.getId()+"/picture",
//                        null,
//                        HttpMethod.GET,
//                        new GraphRequest.Callback() {
//                            public void onCompleted(GraphResponse response) {
//                                Log.e(TAG,"zdjęcie!!!");
//                            }
//                        }
//                ).executeAsync();
            }

        }
        else{
            progress.dismiss();
        }
        Log.e(TAG,response.getMessage());
        Log.e(TAG,"user registered");
        Toast.makeText(getActivity(), response.getMessage(),Toast.LENGTH_LONG).show();
    }

    private void handleResponseFB(Response response) {
        //mProgressbar.setVisibility(View.GONE);
//        Log.e("handleResponse", "in");
//            if(user.getImage()!=null){
//                beginUploadFb();
//            }else{
//                Log.e(TAG, getString(R.string.default_pic_log));
//            }
        progress.dismiss();
        Log.e(TAG,response.getMessage());
        Log.e(TAG,"user registered");
        Toast.makeText(getActivity(), response.getMessage(),Toast.LENGTH_LONG).show();
        startActivity(new Intent(getActivity(), LogInActivity.class));
        getActivity().finish();
    }

    private void handleError(Throwable error) {

        //mProgressbar.setVisibility(View.GONE);

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {
                Log.e(TAG,"In Http Exception");
                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody,Response.class);
                Log.e(TAG,response.getMessage());

                Toast.makeText(getActivity(), response.getMessage(),Toast.LENGTH_LONG).show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG,"Network Error !");
            Toast.makeText(getActivity(), getResources().getString(R.string.networkError),Toast.LENGTH_LONG).show();
        }
        progress.dismiss();
    }

    private void beginUpload(Boolean fb){
        if (fb == false) {
            observer = ImagesHandler.beginUpload(getActivity(), Constants.BUCKET_USER,LogInActivity.picBitmap, pcInfo.getUserPicture());
        }else{
            //po instalacji fb
//            Bitmap bitmap = getFacebookProfilePicture(LogInFragment.profile.getId());
//            Log.d(TAG, "Facebook user");
//            observer = ImagesHandler.beginUpload(getActivity(),Constants.BUCKET_USER,bitmap, pcInfo.getUserPicture());
        }

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    Log.d(TAG, "Upload Completed");
                    progress.dismiss();
                    startActivity(new Intent(getActivity(), LogInActivity.class));
                    getActivity().finish(); //przeniesione z handleResponse

                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {}

            @Override
            public void onError(int id, Exception ex) {}
        });

    }

    public static Bitmap getFacebookProfilePicture(String userID){
        URL imageURL = null;
        try {
            imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }



}
