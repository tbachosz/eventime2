package com.eventime.eventime.screens.users;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.eventime.eventime.R;
import com.eventime.eventime.model.User;
import com.eventime.eventime.utils.Constants;
import com.eventime.eventime.utils.PictureDialog;
import com.eventime.eventime.utils.PictureInfo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subscriptions.CompositeSubscription;

public class RegisterStepOneFragment extends Fragment {

    public static final String TAG = RegisterStepTwoFragment.class.getSimpleName();
    private CompositeSubscription mSubscriptions;
    private ProgressDialog progress;

    public static User user;

    public static ImageButton ibUserImage;
    public static String pictureName = null;
    public static Uri pictureUri = null;
    public PictureInfo pcInfo;

    // With underscore - a button, without - a field
    @BindView(R.id.b_next)
    Button bNext;
    @BindView(R.id.et_full_name)
    EditText etUsername;
    @BindView(R.id.user_image)
    ImageButton imgBUserImage;

    @BindView(R.id.ti_username)
    TextInputLayout tiUsername;

    @OnClick(R.id.user_image)
    public void openPicDialog(){
        PictureDialog fragment = new PictureDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("caller",Constants.Caller.NEW_PROFILE);
        fragment.setArguments(bundle);
        fragment.show(getActivity().getSupportFragmentManager().beginTransaction(), PictureDialog.TAG);
    }

    @OnClick(R.id.b_next)
    public void continueButtonPressed(){

        clearErrors();

        String username;

        username = etUsername.getText().toString();

        if(username.length()>30)
            tiUsername.setError(getResources().getString(R.string.usernameTooLong));
        else if (username.length()<4)
            tiUsername.setError(getResources().getString(R.string.usernameTooShort));
        else{
            user.setName(username);

            if(pictureUri!=null){
                pcInfo = new PictureInfo(pictureUri.toString(),pictureName);
                user.setPicture(Constants.USER_PICTURE_BUCKET + pictureName);
            }else{
                pcInfo = new PictureInfo(null,pictureName);
                user.setPicture(null);
            }
            replaceFragment(1);
        }
    }

    private void clearErrors(){
        tiUsername.setError(null);
    }

    private void replaceFragment(int code) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("userData",user);
        bundle.putParcelable("pcInfo",pcInfo);
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        ft.addToBackStack("RegisterStepOne");

        if(code==0){
            RegisterStepOneFragment fragment = new RegisterStepOneFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, RegisterStepOneFragment.TAG);
        }
        else if(code==1){
            RegisterStepTwoFragment fragment = new RegisterStepTwoFragment();
            fragment.setArguments(bundle);
            ft.replace(R.id.fragmentFrame, fragment, RegisterStepTwoFragment.TAG);
        }

        ft.commit();

    }

    private OnFragmentInteractionListener mListener;

    public RegisterStepOneFragment() {
        // Required empty public constructor
    }

    public static RegisterStepOneFragment newInstance(String param1, String param2) {
        RegisterStepOneFragment fragment = new RegisterStepOneFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(R.string.checking_emial));
        progress.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_register_step_one, container, false);
        ButterKnife.bind(this,view);
        user = getArguments().getParcelable("userData");
        if(user.getFacebook()==null){
            user.setFacebook(false);//we do it here as otherwise taking photos crashes, because this field is a null
        }
        ibUserImage = (ImageButton)view.findViewById(R.id.user_image);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Drawable drawable = ibUserImage.getBackground();
        Bitmap bitmap = drawableToBitmap(drawable);
        outState.putParcelable("image", bitmap);
        super.onSaveInstanceState(outState);
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private void showSnackBarMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

}
