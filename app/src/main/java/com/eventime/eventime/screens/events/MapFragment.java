package com.eventime.eventime.screens.events;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eventime.eventime.R;
import com.eventime.eventime.model.Event;
import com.eventime.eventime.utils.PermissionUtils;
import com.eventime.eventime.utils.PictureInfo;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.eventime.eventime.utils.Constants.LOCATION_REQUEST;

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, LocationListener {
    public static final String TAG = MapFragment.class.getSimpleName();
    private OnFragmentInteractionListener mListener;

    public PictureInfo pcInfo;
    private Event myEvent;

    private MapView mapView;
    private GoogleMap googleMap;
    private Marker marker;
    private Event thisEvent;

    private LocationManager locationManager;

    @BindView(R.id.tv_map_ref)
    TextView tvAddress;

    public MapFragment() {
        // Required empty public constructor
    }

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);
        thisEvent = getArguments().getParcelable("event");
        pcInfo = getArguments().getParcelable("picInfo");

        locationPermission();

        tvAddress.setText(thisEvent.getAddress());

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    private void locationPermission(){
        if (PermissionUtils.requestPermissionFragment(getActivity(), this, LOCATION_REQUEST, Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "onPermissionsResult");
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (PermissionUtils.permissionGranted(requestCode, LOCATION_REQUEST, grantResults)) {
                    findMyLocation();
                }
                break;
        }
    }


    private void findMyLocation() {
        // Access to the location has been granted to the app.
//            googleMap.setMyLocationEnabled(true);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        Location location = locationManager.getLastKnownLocation(locationManager
                .getBestProvider(criteria, false));

        if (location != null) {
            // Do something with the recent location fix
            //  otherwise wait for the update below
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }

    }


    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
//            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
//                    Manifest.permission.ACCESS_FINE_LOCATION, true);
//            gotoLocation(40.712783699999996,-74.0059413, 13);
        } else if (googleMap != null) {
            // Access to the location has been granted to the app.
            googleMap.setMyLocationEnabled(true);
            locationManager = (LocationManager)
                    getActivity().getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            if (location != null){ // && location.getTime() > Calendar.getInstance().getTimeInMillis() - 2 * 60 * 1000) {
                // Do something with the recent location fix
                //  otherwise wait for the update below
//                gotoLocation(location.getLatitude(), location.getLongitude(), 13);
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }

        }
    }

    public void onLocationChanged(Location location) {
//        if (location != null)
//            gotoLocation(location.getLatitude(), location.getLongitude(), 13);

    }

    // Required functions
    public void onProviderDisabled(String arg0) {}
    public void onProviderEnabled(String arg0) {}
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}


    private void gotoLocation(double lat, double lng, float zoom) {
        LatLng latLng = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        googleMap.moveCamera(update);

        addMarker(lat, lng);
    }


    @Override
    public final void onStart (){
        super.onStart();
    }

    @Override
    public final void onResume(){
        super.onResume();
    }

    @Override
    public final void onPause(){
        super.onPause();
    }

    @Override
    public final void onStop(){
        super.onStop();
    }

    @Override
    public final void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public final void onLowMemory(){
        super.onLowMemory();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        googleMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();
        initMap();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private void initMap() {

        if (googleMap != null) {
            googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    return null;
                }
            });

            googleMap.setOnMarkerClickListener(marker1 -> {
                String msg = marker1.getTitle() + " (" +
                        marker1.getPosition().latitude + ", " +
                        marker1.getPosition().longitude + ")";
                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                return false;
            });

            gotoLocation(thisEvent.getLocation().getLocation_lat(),thisEvent.getLocation().getLocation_lon(),13);

        }

    }

    private void addMarker(double ltt, double lng) {

        if (marker != null) {
            removeEverything();
        }

        LatLng latLng = new LatLng(ltt, lng);
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker());
//              .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker));

        marker = googleMap.addMarker(options);

    }

    private void removeEverything() {
        marker.remove();
        marker = null;
    }

}
