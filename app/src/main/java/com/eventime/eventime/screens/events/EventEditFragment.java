package com.eventime.eventime.screens.events;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.eventime.eventime.R;
import com.eventime.eventime.screens.MainActivity;
import com.eventime.eventime.model.Event;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.utils.Constants;
import com.eventime.eventime.utils.PictureDialog;
import com.eventime.eventime.utils.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EventEditFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EventEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventEditFragment extends Fragment {
    public static final String TAG = EventEditFragment.class.getSimpleName();
    private OnFragmentInteractionListener mListener;
    public static Event thisEvent;
    public CompositeSubscription mSubscriptions;
    public static Uri pictureUri=null;
    public static ImageButton ibEventImage;
    // The TransferUtility is the primary class for managing transfer to S3
    private TransferUtility transferUtility;

    private ArrayAdapter<CharSequence> adapterType;
    private ArrayAdapter<CharSequence> adapterTopic;

    @BindView(R.id.b_save)
    Button bSave;
    @BindView(R.id.b_delete)
    Button bDelete;
    @BindView(R.id.tv_name)
    TextView tvEventName;
    @BindView(R.id.s_type)
    Spinner spEventType;
    @BindView(R.id.s_topic)
    Spinner spEventTopic;
    @BindView(R.id.tv_host)
    TextView tvEventHost;
//    @BindView(R.id.tv_users_disclaimer)
//    TextView tvUsersDisclaimer;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.imb_change_image)
    ImageButton imbChangeImage;
    @BindView(R.id.imb_map)
    ImageButton imbOpenMap;
    @BindView(R.id.iv_event_image)
    ImageView ivEventImage;

    @OnClick(R.id.b_save)
    public void returnToView(){
        save();
    }

    @OnClick(R.id.b_delete)
    public void delete(){
        mSubscriptions.add(NetworkUtil.getRetrofit().editEvent(thisEvent.getId(), thisEvent)//Adding a dummy user to send FCM (otherwise, we cannot send strings)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse2, this::handleError));
    }

    @OnClick(R.id.imb_change_image)
    public void changeImage(){
        PictureDialog fragment = new PictureDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("caller",Constants.Caller.EDIT_EVENT);//if caller is true dialog comes from registration, if false then from newEvent
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(), PictureDialog.TAG);
    }

    public EventEditFragment() {
        // Required empty public constructor
    }

    public static EventEditFragment newInstance(String param1, String param2) {
        EventEditFragment fragment = new EventEditFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        transferUtility = Util.getTransferUtility(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_edit, container, false);
        ButterKnife.bind(this, view);
        ButterKnife.bind(this,view);
        thisEvent = getArguments().getParcelable("event");
        Log.e("Event view", thisEvent.toString());
        tvDescription.setText(thisEvent.getDescription());
        tvEventName.setText(thisEvent.getName());
//        tvEventHost.setText(thisEvent.getHost());
//        tvUsersDisclaimer.setText(thisEvent.getTotalOfUsersInterested());
        mSubscriptions = new CompositeSubscription();
        initSpinners();
        return view;
    }

    public void save(){
        if(!thisEvent.getDescription().equals(tvDescription.getText().toString()))
            thisEvent.setDescription(tvDescription.getText().toString());

        if(!thisEvent.getName().equals(tvEventName.getText().toString()))
            thisEvent.setName(tvEventName.getText().toString());

        if(!thisEvent.getType().equals(spEventType.getSelectedItem().toString()))
            thisEvent.setType(spEventType.getSelectedItem().toString());

//        if(!thisEvent.getTopic().equals(spEventTopic.getSelectedItem().toString()))
//            thisEvent.setTopic(spEventTopic.getSelectedItem().toString());
        //Picture is done in the PictureDialog. Uploads only when changed, name remains the same, so there is nothing to do here in terms of the Event.
        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).editEvent(thisEvent.getId(), thisEvent)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }

    private void handleResponse(Response response) {
        if(response.getToken() != null)
            Constants.saveAccessToken(getActivity(), response.getToken());

        if(pictureUri!=null){
            beginUpload();
        }
        replaceFragment(0);
    }

    private void handleResponse2(Response response) {
        startActivity(new Intent(getActivity(), MainActivity.class));
        getActivity().finish();
    }

    private void showSnackBarMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private void handleError(Throwable error) {

        Log.e(TAG, "Login error!: " + error.getMessage());

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            showSnackBarMessage("Network Error !");
        }
    }

    private void replaceFragment(int code) {

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.addToBackStack("EventEditFragment");

        if(code==0){
            Bundle b = new Bundle();
            b.putParcelable("event",thisEvent);
            EventViewFragment fragment = new EventViewFragment();
            fragment.setArguments(b);
            ft.replace(R.id.fragmentFrame, fragment, EventViewFragment.TAG);
        }
        ft.commit();
    }


    public void initSpinners(){
        adapterType = ArrayAdapter.createFromResource(getActivity(), R.array.affilations, android.R.layout.simple_spinner_item);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEventType.setAdapter(adapterType);
        spEventType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //userGender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spEventType.setSelection(adapterType.getPosition(thisEvent.getType().toString()));

        adapterTopic = ArrayAdapter.createFromResource(getActivity(), R.array.affilations, android.R.layout.simple_spinner_item);
        adapterTopic.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEventTopic.setAdapter(adapterTopic);
        spEventTopic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //userGender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        spEventTopic.setSelection(adapterTopic.getPosition(thisEvent.getTopic().toString()));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Drawable drawable = ibEventImage.getBackground();
        Bitmap bitmap = drawableToBitmap(drawable);
        outState.putParcelable("image", bitmap);
        super.onSaveInstanceState(outState);
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private void beginUpload(){
        Log.e(TAG,pictureUri.toString());
        //Uri myUri = Uri.parse(pictureUri+thisEvent.getImage());
        TransferObserver observer = transferUtility.upload(Constants.BUCKET_EVENT, thisEvent.getImage(),
                storeImage(CreateNewEventActivity.picBitmap,thisEvent.getImage()));
    }


    public static File storeImage(Bitmap bm, String fileName){
        File dir = new File(Constants.dirPathMyPics);
        if(!dir.exists())
            dir.mkdirs();
        File file = new File(Constants.dirPathMyPics, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
            Log.e("SAVED", "done");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
}
