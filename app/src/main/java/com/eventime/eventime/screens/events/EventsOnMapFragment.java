package com.eventime.eventime.screens.events;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eventime.eventime.R;
import com.eventime.eventime.model.Event;
import com.eventime.eventime.utils.PermissionUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;

import static com.eventime.eventime.utils.Constants.LOCATION_REQUEST;

/**
 * Created by Cipson on 2017-10-12.
 */

public class EventsOnMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, LocationListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener {
    public static final String TAG = EventsOnMapFragment.class.getSimpleName();
    private EventsOnMapFragment.OnFragmentInteractionListener mListener;

    private MapView mapView;
    private GoogleMap googleMap;

    private LocationManager locationManager;

    private Marker[] markers;

    public static ArrayList<Event> eventList;

    private View view;

    public EventsOnMapFragment() {
        // Required empty public constructor
    }

    public static EventsOnMapFragment newInstance() {
        EventsOnMapFragment fragment = new EventsOnMapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_events_on_map, container, false);
        ButterKnife.bind(this, view);
        eventList = getArguments().getParcelableArrayList("event_list");

        if(eventList != null)
            markers = new Marker[eventList.size()];

        locationPermission();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    private void replaceFragment(int code, Event event, int position) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Bundle b = new Bundle();
        b.putParcelable("event",event);
        b.putInt("position",position);

        ft.addToBackStack("EventsOnMap");

        if(code==0){
            EventViewFragment fragment = new EventViewFragment();
            fragment.setArguments(b);
            ft.replace(R.id.fragmentFrame, fragment, EventViewFragment.TAG);
        }

        ft.commit();
    }


    private void locationPermission(){
        if (PermissionUtils.requestPermissionFragment(getActivity(), this, LOCATION_REQUEST, Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "onPermissionsResult");
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (PermissionUtils.permissionGranted(requestCode, LOCATION_REQUEST, grantResults)) {
                    findMyLocation();
                }
                break;
        }
    }


    private void findMyLocation() {
        // Access to the location has been granted to the app.
//            googleMap.setMyLocationEnabled(true);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        Location location = locationManager.getLastKnownLocation(locationManager
                .getBestProvider(criteria, false));

        if (location != null) {
            // Do something with the recent location fix
            //  otherwise wait for the update below
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }

    }


    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
//            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
//                    Manifest.permission.ACCESS_FINE_LOCATION, true);
//            gotoLocation(40.712783699999996,-74.0059413, 13);
        } else if (googleMap != null) {
            // Access to the location has been granted to the app.
            googleMap.setMyLocationEnabled(true);
            locationManager = (LocationManager)
                    getActivity().getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            if (location != null){ // && location.getTime() > Calendar.getInstance().getTimeInMillis() - 2 * 60 * 1000) {
                // Do something with the recent location fix
                //  otherwise wait for the update below
                gotoLocation(location.getLatitude(), location.getLongitude(), 13);
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }

        }
    }

    public void onLocationChanged(Location location) {
        if (location != null)
            gotoLocation(location.getLatitude(), location.getLongitude(), 13);

    }

    // Required functions
    public void onProviderDisabled(String arg0) {}
    public void onProviderEnabled(String arg0) {}
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {}


    private void gotoLocation(double lat, double lng, float zoom) {
        LatLng latLng = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        googleMap.moveCamera(update);
    }


    @Override
    public final void onStart (){
        super.onStart();
    }

    @Override
    public final void onResume(){
        super.onResume();
    }

    @Override
    public final void onPause(){
        super.onPause();
    }

    @Override
    public final void onStop(){
        super.onStop();
    }

    @Override
    public final void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public final void onLowMemory(){
        super.onLowMemory();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        googleMap.setOnMyLocationButtonClickListener(this);

        enableMyLocation();
        initMap();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private void initMap() {

        if (googleMap != null) {
            googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    return null;
                }
            });

            googleMap.setOnMarkerClickListener(marker1 -> {
                String msg = marker1.getTitle() + " (" +
                        marker1.getPosition().latitude + ", " +
                        marker1.getPosition().longitude + ")";
                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                return false;
            });

            addMarkers();

        }

    }

    private void addMarkers(){

        if(eventList != null)
            for(int i = 0; i < eventList.size(); i++){
                markers[i] = googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(eventList.get(i).getLocation_lat(), eventList.get(i).getLocation_lon()))
                        .title(eventList.get(i).getName())
//                        .snippet(eventList.get(i).getType())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                    GoogleMap.InfoWindowAdapter infoWindowAdapter = new GoogleMap.InfoWindowAdapter() {
                        @Override
                        public View getInfoWindow(Marker marker) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker marker) {
                            View v = getActivity().getLayoutInflater().inflate(R.layout.event_info_window, null);

                            for (int i = 0; i < markers.length; i++) {
                                Event event = eventList.get(i);

                                    if (markers[i].getId().equals(marker.getId())) {
                                        TextView name = (TextView) v.findViewById(R.id.textEventName);
                                        name.setText(event.getName());

                                        TextView type = (TextView) v.findViewById(R.id.textEventType);
                                        type.setText(event.getType());

                                        TextView startDate = (TextView) v.findViewById(R.id.textStartDate);

//                                        String time= checkTime(event.getStartTimeHour().getN())+ ":"+ checkTime(event.getStartTimeMinute().getN()) + " " + checkTime(event.getStartDateDay().getN())+"/"+checkTime(event.getStartDateMonth().getN())+"/"+event.getStartDateYear();

//                                        startDate.setText(timeLeft(time, event));


                                        ImageView imageView = (ImageView) v.findViewById(R.id.eventImage);
                                        if (event.getImage() == null) {
//                                            String uri = "@drawable/ic_landscape_black_48dp";  // where myresource (without the extension) is the file
//
//                                            int imageResource = EventsMapActvity.this.getApplicationContext().getResources().getIdentifier(uri, null, EventsMapActvity.this.getApplicationContext().getPackageName());
//
//                                            Drawable res = EventsMapActvity.this.getApplicationContext().getResources().getDrawable(imageResource);
//                                            imageView.setImageDrawable(res);
                                        } else {
                                            Picasso.with(getActivity().getApplicationContext()).load(event.getImage()).into(imageView);
                                        }
                                    }
                            }

//                        nameView.setText(name);
                            return v;
                        }
                    };

//
                    googleMap.setOnMarkerClickListener(this);
                    googleMap.setOnInfoWindowClickListener(this);
                    googleMap.setInfoWindowAdapter(infoWindowAdapter);


            }

//        googleMap.setMyLocationEnabled(true);

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if(marker == null) {
            Log.e("Marker", "null");
            return;
        }else
            Log.e("Marker", marker.getId());

        for(int i= 0; i < markers.length; i++) {
            if(markers[i] != null && markers[i].getId().equals(marker.getId())) {
                replaceFragment(0, eventList.get(i), i);
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return true;
    }

}
