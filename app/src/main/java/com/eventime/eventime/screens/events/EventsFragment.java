package com.eventime.eventime.screens.events;

////import android.app.FragmentTransaction;
//import android.app.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.eventime.eventime.R;
import com.eventime.eventime.screens.users.UserProfileFragment;
import com.eventime.eventime.model.Event;
import com.eventime.eventime.model.EventList;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.utils.Constants;
import com.eventime.eventime.utils.FiltersDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EventsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EventsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventsFragment extends Fragment {
    public static final String TAG = EventsFragment.class.getSimpleName();

    private CompositeSubscription mSubscriptions;
    private RecyclerView.LayoutManager mLayoutManager;

    public static ArrayList<Event> eventList;
    private ProgressDialog progress;
    @BindView(R.id.tv_progressText)
    TextView progressText;
    @BindView(R.id.linlaHeaderProgress)
    LinearLayout headerProgress;
    @BindView(R.id.pbHeaderProgress)
    ProgressBar pbHeaderProgress;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    public static RecyclerView lvEvents;
//    @BindView(R.id.list)
//    ListView lvEvents;
    @BindView(R.id.new_event_button)
    FloatingActionButton addNewEvent;
    @BindView(R.id.sv_event)
    SearchView svEvent;
    @BindView(R.id.b_open_filters)
    ImageButton bOpenFilters;

    @OnClick(R.id.b_open_filters)
    public void openDialogFilters(){
//        PictureDialog fragment = new PictureDialog();
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("caller",Constants.Caller.NEW_ORGANIZATION);//if caller is true dialog comes from registration, if false then from newEvent
//        fragment.setArguments(bundle);
//        fragment.show(getActivity().getSupportFragmentManager().beginTransaction(), PictureDialog.TAG);
        FiltersDialog fragment = new FiltersDialog();
        fragment.show(getActivity().getSupportFragmentManager().beginTransaction(), FiltersDialog.TAG);
    }

    @OnClick(R.id.new_event_button)
    public void createNewEvent(){
        startActivity(new Intent(getActivity(),CreateNewEventActivity.class));
    }

//    @OnClick(R.id.b_map)
//    public void openMap(){
//        Bundle bundle = new Bundle();
//        bundle.putParcelableArrayList("event_list", eventList);
//        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//
//        ft.addToBackStack("LogInFragment");
//
//        EventsOnMapFragment fragment = new EventsOnMapFragment();
//        fragment.setArguments(bundle);
//        ft.replace(R.id.fragmentFrame, fragment, EventsOnMapFragment.TAG);
//
//        ft.commit();
//    }
//
//    @OnClick(R.id.b_chats)
//    public void openChats(){
//        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//
//        ft.addToBackStack("LogInFragment");
//
//        ChatListFragment fragment = new ChatListFragment();
//        ft.replace(R.id.fragmentFrame, fragment, ChatListFragment.TAG);
//
//        ft.commit();
//    }

    private OnFragmentInteractionListener mListener;

    public EventsFragment() {
        // Required empty public constructor
    }

    public static EventsFragment newInstance(String param1, String param2) {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        lvEvents = (RecyclerView) view.findViewById(R.id.list);
        ButterKnife.bind(this,view);

        getEvents();

//        lvEvents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position,
//                                    long id) {
//                Event currEvent = (Event) parent.getAdapter().getItem(position);
//                Log.e(TAG, currEvent.getName().toString());
//                replaceFragment(0, currEvent, position);
//            }
//        });
//
//        lvEvents.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                lvEvents.setSelection(0);
//            }
//        }, 500);



        progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(R.string.searching));
        progress.setCancelable(false);

        initSearchView();
        initSwipeRefresh();
        return view;
    }


    private void replaceToProfile(){
        Bundle bundle = new Bundle();
        bundle.putString("email", "nipek1@op.pl");
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        ft.addToBackStack("LogInFragment");

        UserProfileFragment fragment = new UserProfileFragment();
        fragment.setArguments(bundle);
        ft.replace(R.id.fragmentFrame, fragment, UserProfileFragment.TAG);

        ft.commit();
    }

    private void replaceFragment(int code, Event event, int position) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Bundle b = new Bundle();
        b.putParcelable("event",event);
        b.putInt("position",position);

        ft.addToBackStack("EventsFragment");

        if(code==0){
            EventViewFragment fragment = new EventViewFragment();
            fragment.setArguments(b);
            ft.replace(R.id.fragmentFrame, fragment, EventViewFragment.TAG);
        }
//        else if(code==1){
//            EventEditFragment fragment = new EventEditFragment();
//            ft.replace(R.id.fragmentFrame, fragment, EventEditFragment.TAG);
//        }
        ft.commit();
    }

    public static EventsFragment getInstance(){
        EventsFragment fragment = new EventsFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    private void getEvents() {
        //Change latter to loggeduser.getEmail()
        //for now not sure if it already download these data

        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).getEvents(Constants.loggedUser.getEmail())
                .observeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }

    public void initSwipeRefresh() {
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                Log.v("swipeContainer", "REFRESH!");
                refresh();
            }
        });
        swipeContainer.setColorSchemeResources(new int[]{17170456});
    }

    public void refresh() {
        if(eventList == null) {
            pbHeaderProgress.setVisibility(View.VISIBLE);
            headerProgress.setVisibility(View.VISIBLE);
            progressText.setText(getResources().getString(R.string.loading_events));
        }
            getEvents();
    }


    private void handleResponse(EventList events) {
        progress.dismiss();

        Log.e(TAG, "Events!");

        if(events.getToken() != null)
            Constants.saveAccessToken(getActivity(), events.getToken());

        eventList = events.getEvents();

       EventAdapter adapter = new EventAdapter(eventList, getContext(),getActivity());

        //lvEvents.setHasFixedSize(true);

        // use a linear layout manager
        lvEvents.setAdapter(adapter);
        mLayoutManager = new LinearLayoutManager(getActivity());//, LinearLayoutManager.VERTICAL, true);
        lvEvents.setLayoutManager(mLayoutManager);

        if(eventList==null){
            progressText.setText(getResources().getString(R.string.nothing_found));
            pbHeaderProgress.setVisibility(View.GONE);
        }else{
            headerProgress.setVisibility(View.GONE);
            pbHeaderProgress.setVisibility(View.GONE);
        }
        swipeContainer.setRefreshing(false);

//        ItemClickSupport.addTo(lvEvents).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
//            @Override
//            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                Event currEvent = adapter.getElement(position);
//                Log.e(TAG, currEvent.getName().toString());
//                replaceFragment(0, currEvent, position);
//            }
//        });
    }

    private void handleError(Throwable error) {
        progress.dismiss();

        Log.e(TAG, "Events error!: " + error.getMessage());

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private void initSearchView(){
        svEvent.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                getSearchedEvents(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        svEvent.setOnCloseListener(() -> {
            progress.show();
            getEvents();
            return false;
        });
    }

    private void getSearchedEvents(String query) {
        progress.show();

        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).getEventsSearch(query)
                .observeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseSearchedEvents, this::handleError));
    }

    private void handleResponseSearchedEvents(EventList events) {
        progress.dismiss();

        Log.e(TAG, "Events!");

        if(events.getToken() != null)
            Constants.saveAccessToken(getActivity(), events.getToken());

        if(events.getEvents()==null || events.getEvents().size() == 0)
            eventList = new ArrayList<>();
        else
            eventList = events.getEvents();

        EventAdapter adapter = new EventAdapter(eventList, getContext(), getActivity());
        lvEvents.setAdapter(adapter);

    }

}
