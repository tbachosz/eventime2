package com.eventime.eventime.screens.users;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.eventime.eventime.R;
//import com.eventime.eventime.screens.users.Chat.ChatMessagesFragment;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.model.User;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Cipson on 2017-10-10.
 */

public class UserProfileFragment extends Fragment {

    public static final String TAG = UserProfileFragment.class.getSimpleName();
    private CompositeSubscription mSubscriptions;

    private String email;

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.b_chat)
    Button bChat;

    @OnClick(R.id.b_chat)
    public void openChat(){
        replaceFragment(0);
    }

    private void replaceFragment(int code) {

        Bundle bundle = new Bundle();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        bundle.putString("email", email);
        ft.addToBackStack("UserProfileFragment");

//        if (code == 0) {
//            ChatMessagesFragment fragment = new ChatMessagesFragment();
//            fragment.setArguments(bundle);
//            ft.replace(R.id.fragmentFrame, fragment, ChatMessagesFragment.TAG);
//        }

        ft.commit();
    }

    private UserProfileFragment.OnFragmentInteractionListener mListener;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    public static UserProfileFragment newInstance(String param1, String param2) {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        ButterKnife.bind(this, view);

        email= getArguments().getString("email","");
        if(email!= null && !email.equals("")){
            loadProfile();
        }
        //logged user
        else{
            email = Constants.loggedUser.getEmail();
            fillUserData(Constants.loggedUser);
        }

        return view;
    }

    private void fillUserData(User user){

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void loadProfile() {
        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()),  Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).getProfile(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::handleError));
    }


    private void handleResponse(User user) {
        if(user.getToken() != null)
            Constants.saveAccessToken(getActivity(), user.getToken());

        fillUserData(user);
    }

    private void handleError(Throwable error) {
        Log.e(TAG, "Email error!: " + error.getMessage());

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            showSnackBarMessage("Network Error !");
        }
    }

    private void showSnackBarMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }
}
