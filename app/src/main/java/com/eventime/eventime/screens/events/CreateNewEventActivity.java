package com.eventime.eventime.screens.events;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.eventime.eventime.R;
import com.eventime.eventime.model.Event;

import butterknife.ButterKnife;

public class CreateNewEventActivity extends AppCompatActivity {

    public static final String TAG = CreateNewEventActivity.class.getSimpleName();
    public static Bitmap picBitmap;
    public static Uri picUri;
    public static String picName;
    public static Event myEvent = new Event();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_new_event);
        ButterKnife.bind(this);
        replaceFragment();
    }
//ODKOMENTOWAC
    private void replaceFragment() {
//        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
//        NewEventStepOneFragment fragment = new NewEventStepOneFragment();
//        ft.replace(R.id.fragmentFrame, fragment, NewEventStepOneFragment.TAG);
//        ft.commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // below line to be commented to prevent crash on nougat.
        // http://blog.sqisland.com/2016/09/transactiontoolargeexception-crashes-nougat.html
        //
        //super.onSaveInstanceState(outState);
    }


    @Override
    public void onBackPressed() {
        // Catch back action and pops from backstack
        // (if you called previously to addToBackStack() in your transaction)
        if (getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStack();
        }
        // Default action on back pressed
        else super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static CreateNewEventActivity getInstance(){
        CreateNewEventActivity activity = new CreateNewEventActivity();
        return activity;
    }
}
