package com.eventime.eventime.screens.users;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.eventime.eventime.R;
import com.eventime.eventime.screens.events.EventsFragment;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.model.User;
import com.eventime.eventime.network.NetworkUtil;
import com.eventime.eventime.utils.Constants;
import com.eventime.eventime.utils.ImagesHandler;
import com.eventime.eventime.utils.PictureDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class EditProfileFragment extends Fragment implements ChangePasswordDialog.Listener {

    public static ImageView ivUserImage;
    private SharedPreferences mSharedPreferences;
    private static String mToken;
    private String mEmail;
    private String mRefreshToken;
    public static User user = Constants.loggedUser;
    public static Uri pictureUri=null;
    private ProgressDialog progress;

    private CompositeSubscription mSubscriptions;

//    @BindView(R.id.iv_user_image)
//    ImageView ivUserImage;
    @BindView(R.id.bt_change_password)
    Button btChangePassword;
    @BindView(R.id.tv_username)
    TextView tvUserName;
    @BindView(R.id.tv_user_email)
    TextView tvUserEmail;
    @BindView(R.id.tv_user_gender)
    TextView tvUserGender;
    @BindView(R.id.tv_text_birthday)
    TextView tvBirthday;
    @BindView(R.id.addImageButton)
    FloatingActionButton fabImage;

    @OnClick(R.id.addImageButton)
    public void openPicDialog(){
        PictureDialog fragment = new PictureDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("caller",Constants.Caller.EDIT_PROFILE);
        fragment.setArguments(bundle);
        fragment.show(getActivity().getSupportFragmentManager().beginTransaction(), PictureDialog.TAG);
    }

    @OnClick(R.id.b_update_user_data)
    public void onUpdateClick(){
        updateProfile();
    }

    @OnClick(R.id.bt_change_password)
    public void changePassword(){
        ChangePasswordDialog fragment = new ChangePasswordDialog();

        Bundle bundle = new Bundle();
        bundle.putString(Constants.EMAIL, mEmail);
        bundle.putString(Constants.TOKEN, mToken);
        bundle.putString(Constants.REFRESH_TOKEN, mRefreshToken);
        fragment.setArguments(bundle);

        fragment.show(getFragmentManager(), ChangePasswordDialog.TAG);
    }

    public static final String TAG = EventsFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    private void initSharedPreferences() {//To access user's data
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mToken = mSharedPreferences.getString(Constants.TOKEN,"");
        mEmail = mSharedPreferences.getString(Constants.EMAIL,"");
        mRefreshToken = mSharedPreferences.getString(Constants.REFRESH_TOKEN,"");
    }


    public static EditProfileFragment newInstance(String param1, String param2) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//        }
        mSubscriptions = new CompositeSubscription();
        initSharedPreferences();
        progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(R.string.registration_progress));
        progress.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this,view);

        tvUserName.setText(user.getName());
        tvUserEmail.setText(user.getEmail());
        tvUserGender.setText(user.getGender());
        tvBirthday.setText(user.getBirthday());
        ivUserImage=(ImageView) view.findViewById(R.id.iv_user_image);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public static EditProfileFragment getInstance(){
        EditProfileFragment fragment = new EditProfileFragment();
        return fragment;
    }

    @Override
    public void onPasswordChanged() {
        showSnackBarMessage(getString(R.string.change_success));
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);


    }

    public void updateProfile(){
        progress.show();
        mToken = mSharedPreferences.getString(Constants.TOKEN,"");
        mRefreshToken = mSharedPreferences.getString(Constants.REFRESH_TOKEN,"");

        mSubscriptions.add(NetworkUtil.getRetrofit(mToken, mRefreshToken, Constants.getEmail(getActivity())).editProfile(mEmail, new User())//tutaj dane ktore sie zmienily
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::handleError));
    }

    private void handleResponse(Response response) {
        if(pictureUri!=null) {
            beginUpload();
        }

    }

    private void handleError(Throwable error) {
        progress.dismiss();
        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {
                Log.e("HEADERS", ((HttpException) error).response().code() + "");
                if(((HttpException) error).response().code()== 401) {
                    //LOGOUT!!! //mozna zmienic nr kodu ale to trzeba przemyslec - to oznacza ze refresh token juz nie jest valid
                }
                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody,Response.class);
                showSnackBarMessage(response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            showSnackBarMessage(getResources().getString(R.string.profile_update_error));
        }
    }

    private void showSnackBarMessage(String message) {
        Snackbar.make(getView(),message,Snackbar.LENGTH_SHORT).show();
    }

    private void beginUpload() {
        TransferObserver observer;
        observer = ImagesHandler.beginUpload(getActivity(), Constants.BUCKET_USER, LogInActivity.picBitmap, user.getPicture());

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    Log.d(TAG, "Upload Completed");
                    showSnackBarMessage(getResources().getString(R.string.profile_updated));
                    progress.dismiss();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {}

            @Override
            public void onError(int id, Exception ex) {}
        });

    }
}
