package com.eventime.eventime.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;

public class User implements Parcelable {

    private String name,
            email,
            password,
            created_at,
            newPassword,
            token,
            birthday,
            gender;
    private Boolean isFacebook;

    private String picture;


    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    private String fcm_token;

    public User(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public Boolean getFacebook() {
        return isFacebook;
    }

    public void setFacebook(Boolean facebook) {
        isFacebook = facebook;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public User() {
    }

    protected User(Parcel in) {
        name = in.readString();
        email = in.readString();
        password = in.readString();
        created_at = in.readString();
        newPassword = in.readString();
        token = in.readString();
        birthday = in.readString();
        gender = in.readString();
        picture = in.readString();
        isFacebook = in.readByte() != 0;
        fcm_token = in.readString();
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(created_at);
        dest.writeString(newPassword);
        dest.writeString(token);
        dest.writeString(picture);
        dest.writeString(gender);
        dest.writeByte((byte) (isFacebook ? 1 : 0));
}

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public String toString() {
        return "Name: " + name;
    }
}