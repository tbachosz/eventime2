package com.eventime.eventime.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Cipson on 2017-08-07.
 */

public class EventList {
    @SerializedName("events")
    private ArrayList<Event> events;

    public EventList(){}

    public ArrayList<Event> getEvents(){return events;}

    public void setEvents(ArrayList<Event> events){
        this.events = events;
    }

    @SerializedName("token")
    private String token;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken(){return token;}
}
