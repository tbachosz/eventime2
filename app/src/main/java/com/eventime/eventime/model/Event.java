package com.eventime.eventime.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Cipson on 2017-08-06.
 */

public class Event implements Parcelable {

    @SerializedName("token")
    private String token;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken(){return token;}

    private String id;
    private String name;
    private String description;
    private String address;
    private String type;
    private String image;
//            location,

    private List<String> users_interested,
            users_attending;

    private long start_time;

    private User user_host;

//    private float location_lon,
//            location_lat;

    private MyLocationLatLon location = new MyLocationLatLon();

//            start_date,  //TO DISCUSS!!!!!
//            end_date,

    public Event(){}

    public Event(String name, String description, String type, String topic, String image, String address, MyLocationLatLon location, long start_time){
        this.name = name;
        this.description = description;
        this.image = image;
        this.location = location;
        this.start_time = start_time;
        this.address = address;
        this.type = type;
    }

    protected Event(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        image = in.readString();
//        users_attending = in.readArrayList(String.class.getClassLoader());
//        users_interested = in.readArrayList(String.class.getClassLoader());
        location.setLocation_lon(in.readFloat());
        location.setLocation_lat(in.readFloat());
        address = in.readString();
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(address);
        dest.writeString(type);
//        dest.writeList(users_attending);
//        dest.writeList(users_interested);
        dest.writeDouble(location.getLocation_lon());
        dest.writeDouble(location.getLocation_lat());
        dest.writeSerializable((Serializable) user_host);// MOŻE NIE DZIAŁAĆ
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public int getTotalOfUsersInterested(){
        int total;
        if(users_attending==null||users_interested==null){
           total = 0;
        }
        else{
            total=users_interested.size()+users_attending.size();
        }
        return total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

//    public List<String> getUsers_attending() {
//        return users_attending;
//    }
//
//    public void setUsers_attending( List<String> users_attending) {
//        this.users_attending = users_attending;
//    }
//
//    public List<String> getUsers_interested() {
//        return users_interested;
//    }
//
//    public void setUsers_interested( List<String> users_interested) {
//        this.users_interested = users_interested;
//    }


    public double getLocation_lon() {
        return location.getLocation_lon();
    }

    public void setLocation_lon(double location_lon) {
        this.location.setLocation_lon(location_lon);
    }

    public double getLocation_lat() {
        return location.getLocation_lat();
    }

    public void setLocation_lat(double location_lat) {
        this.location.setLocation_lat(location_lat);
    }

    public MyLocationLatLon getLocation() {
        return location;
    }

    public void setLocation(MyLocationLatLon location) {
        this.location = location;
    }

    public long getStartTime() {
        return start_time;
    }

    public void setStartTime(long start_time) {
        this.start_time = start_time;
    }

    public String toString(){
        return "Lat: " + location.getLocation_lat() + " Lon: " + location.getLocation_lon() + name;
    }

    public User getHost() {
        return user_host;
    }

    public void setHost(User host) {
        this.user_host = host;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
