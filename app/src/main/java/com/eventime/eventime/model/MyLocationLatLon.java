package com.eventime.eventime.model;

/**
 * Created by Cipson on 2017-09-10.
 */


public class MyLocationLatLon {

    private double lon, lat;

    public MyLocationLatLon(){}

    public MyLocationLatLon(double location_lon, double location_lat){
        this.lat = location_lat;
        this.lon = location_lon;
    }


    public double getLocation_lon() {
        return lon;
    }

    public void setLocation_lon(double location_lon) {
        this.lon = location_lon;
    }

    public double getLocation_lat() {
        return lat;
    }

    public void setLocation_lat(double location_lat) {
        this.lat = location_lat;
    }


}
