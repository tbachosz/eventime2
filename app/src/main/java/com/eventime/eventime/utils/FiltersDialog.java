package com.eventime.eventime.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.eventime.eventime.R;
import com.eventime.eventime.screens.events.EventAdapter;
import com.eventime.eventime.screens.events.EventsFragment;
import com.eventime.eventime.screens.users.ChangePasswordDialog;
import com.eventime.eventime.model.Event;
import com.eventime.eventime.model.EventList;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.network.NetworkUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.eventime.eventime.screens.events.EventsFragment.eventList;

/**
 * Created by Tomek on 2017-11-20.
 */

public class FiltersDialog extends DialogFragment {

    public static final String TAG = FiltersDialog.class.getSimpleName();

    private ChangePasswordDialog.Listener mListener;
    private CompositeSubscription mSubscriptions;
    private SharedPreferences mSharedPreferences;

    private Context globalContext;

    private ArrayList<String> issues = new ArrayList<>();
    private Event filterEvent = new Event();

//    private ChipCloud chipCloud;

    @BindView(R.id.b_filter)
    Button bFilter;

    @BindView(R.id.actv_add_tags)
    AutoCompleteTextView addTags;
    @BindView(R.id.tv_tags)
    TextView tvTags;
    @BindView(R.id.lv_issues)
    ListView lvIssues;

    @OnClick(R.id.b_filter)
    public void onClick(){
        //filterEvent.setTopic();
//        filterEvent.setTags(issues);
        Log.e(TAG,"Filter clicked!");
//        getDialog().dismiss();

        mSubscriptions.add(NetworkUtil.getRetrofit(Constants.getAccessToken(getActivity()), Constants.getRefreshToken(getActivity()), Constants.getEmail(getActivity())).getFilteredEvents(filterEvent)
                .observeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));

    }

    private void handleResponse(EventList events) {

        Log.e(TAG, "Events!");

        if(events.getToken() != null)
                Constants.saveAccessToken(getActivity(), events.getToken());

        if(events.getToken() != null)
            Constants.saveAccessToken(getActivity(), events.getToken());

        if(events.getEvents()==null || events.getEvents().size() == 0)
            eventList = new ArrayList<>();
        else
            eventList = events.getEvents();

        Log.e("Events downloaded",eventList.toString());
        EventAdapter adapter = new EventAdapter(eventList, getContext(), getActivity());
        EventsFragment.lvEvents.setAdapter(adapter);

        EventsFragment.lvEvents.setAdapter(adapter);
        getDialog().dismiss();
//Check if we really ahve to set up the adapter
//        EventAdapter adapter = new EventAdapter(getActivity().getBaseContext(), R.layout.item_event ,events.getEvents());
//            lvEvents.setAdapter(adapter);
//
//            if(eventList==null){
//            progressText.setText(getResources().getString(R.string.nothing_found));
//            pbHeaderProgress.setVisibility(View.GONE);
//        }else{
//            headerProgress.setVisibility(View.GONE);
//            pbHeaderProgress.setVisibility(View.GONE);
//        }
//            swipeContainer.setRefreshing(false);
    }

    private void handleError(Throwable error) {


        Log.e(TAG, "Events error!: " + error.getMessage());

        if (error instanceof HttpException) {

            Gson gson = new GsonBuilder().create();

            try {

                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

        }
        getDialog().dismiss();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        progress = new ProgressDialog(getActivity());
//        progress.setMessage(getString(R.string.registration_progress));
//        progress.setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_filters,container,false);
        mSubscriptions = new CompositeSubscription();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        ButterKnife.bind(this, view);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

//        initTags();
//        initList();

        return view;
    }

//    private void initTags(){
//        String[] tags = getResources().getStringArray(R.array.hashTypes);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,  tags);
//        addTags.setAdapter(adapter);
//
//        addTags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if(tvTags.getText().toString().equals(""))
//                    tvTags.setText(addTags.getText().toString());
//                else
//                    tvTags.setText(tvTags.getText().toString() + ", " + addTags.getText().toString());
//                addTags.setText("");
//                addTagView(addTags.getText().toString());
//            }
//        });
//
//        addTags.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.e("ITEM", "selected");
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//                Log.e("ITEM", "selected nothing");
//            }
//        });
//    }

//    private void initList(){
//        List<String> issueList = Arrays.asList(getResources().getStringArray(R.array.hashTypes));
//
//        IssueAdapter adapter = new IssueAdapter(getActivity().getBaseContext(), R.layout.item_event ,issueList);
//        lvIssues.setAdapter(adapter);
//
//        lvIssues.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position,
//                                    long id) {
//                if(tvTags.getText().toString().equals(""))
//                    tvTags.setText(issueList.get(position));
//                else
//                    tvTags.setText(tvTags.getText().toString() + ", " + issueList.get(position));
//                addTagView(issueList.get(position));
//
//            }
//        });
//    }

    private void addTagView(String text){
        issues.add(text);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        globalContext = context;
//        mListener = (CreateNewEventActivity)context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }
}
