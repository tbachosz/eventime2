package com.eventime.eventime.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Lukas on 2016-09-26.
 */
public class ImagesHandler {

    public static TransferObserver beginUpload(Context context, String bucketName, Bitmap bitmap, String fileName){
        TransferUtility transferUtility = Util.getTransferUtility(context);
        File file = storeImage(bitmap, fileName);

        return transferUtility.upload(bucketName,fileName,
                file);

//        TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, file.getName(),
//                file);
        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
        // observer.setTransferListener(new UploadListener());
    }

    public static File storeImage(Bitmap bm, String fileName){
        File dir = new File(Constants.dirPathMyPics);
        if(!dir.exists())
            dir.mkdirs();
        File file = new File(Constants.dirPathMyPics, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm = ImagesHandler.scaleBitmapDown(bm, 500);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
            Log.d("SAVED", "done");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }


    public static Bitmap scaleBitmapDown(Bitmap bitmap, int maxDimension) {

        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int resizedWidth = maxDimension;
        int resizedHeight = maxDimension;

        if (originalHeight > originalWidth) {
            resizedHeight = maxDimension;
            resizedWidth = (int) (resizedHeight * (float) originalWidth / (float) originalHeight);
        } else if (originalWidth > originalHeight) {
            resizedWidth = maxDimension;
            resizedHeight = (int) (resizedWidth * (float) originalHeight / (float) originalWidth);
        } else if (originalHeight == originalWidth) {
            resizedHeight = maxDimension;
            resizedWidth = maxDimension;
        }
        return Bitmap.createScaledBitmap(bitmap, resizedWidth, resizedHeight, false);
    }


    public static Bitmap rotate (String filePath, Bitmap sourceBitmap) throws IOException {
        Log.e("Stag1","1");
        ExifInterface exif = new ExifInterface(filePath);
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        Log.e("Stag1","2");
        Matrix matrix = new Matrix();
        Log.e("Stag1","3");
        if(orientation==6){
            Log.e("Stag1","4");
            matrix.postRotate(90);}
        else if(orientation==3){
            Log.e("Stag1","5");
            matrix.postRotate(180);
        }

        else if(orientation==8){
            Log.e("Stag1","6");
            matrix.postRotate(270);
        }


        Bitmap rotatedBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);
        Log.e("Stag1","7");
        return rotatedBitmap;
    }


}
