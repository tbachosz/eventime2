package com.eventime.eventime.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.eventime.eventime.model.User;
import com.eventime.eventime.model.User;

public class Constants {

    public static final String BASE_URL = "https://ballot-box-test.herokuapp.com";
    public static final String API_URL = "https://ballot-box-test.herokuapp.com/api/v1/";
            //"https://ballot-box-test.herokuapp.com/api/v1/";
            // "http://192.168.1.108:8080/api/v1/";
    public static final String TOKEN = "token";
    public static final String EMAIL = "email";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String COGNITO_POOL_ID = "us-east-1:20a9c264-b671-47a6-8698-7bda9ea6fdfa";
    public static final String COGNITO_POOL_REGION = "us-east-1";
    public static final String BUCKET_DEBATES = "ballotbox-debates";
    public static final String BUCKET_POLITICIANS = "ballotbox-politicians";
    public static final String BUCKET_USER = "ballotbox-users";
    public static final String BUCKET_EVENT = "ballotbox-event";
    public static final String BUCKET_ORGANIZATIONS = "ballotbox-organizations";
    public static final String BUCKET_REGION = "us-east-1";
    public static final String USER_PICTURE_BUCKET = "https://s3.amazonaws.com/ballotbox-users/";
    public static final String EVENT_PICTURE_BUCKET = "https://s3.amazonaws.com/ballotbox-event/";
    public static final String POLITICIAN_PICTURE_BUCKET = "https://s3.amazonaws.com/ballotbox-politician/";
    public static final String ORGANIZATION_PICTURE_BUCKET = "https://s3.amazonaws.com/ballotbox-organizations/";


    public static final String dirPathMain = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Balltbox/";
    public static final String dirPathMyPics= dirPathMain + "BallotBoxPics/";
//    public static final String dirPathOtherCards = dirPathMain + "OtherCards/";

    public static final int GALLERY_PERMISSIONS_REQUEST = 0;
    public static final int GALLERY_IMAGE_REQUEST = 1;
    public static final int CAMERA_PERMISSIONS_REQUEST = 2;
    public static final int CAMERA_IMAGE_REQUEST = 3;
    public static final int LOCATION_REQUEST = 4;

    public static User loggedUser;

    private static Context context;

    public enum Caller{
        NEW_EVENT, EDIT_EVENT, NEW_PROFILE, EDIT_PROFILE, NEW_ORGANIZATION
    }

    public static void saveTokens(Activity activity, String token, String refreshToken, String email){
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.TOKEN, token);
        editor.putString(Constants.EMAIL, email);
        editor.putString(Constants.REFRESH_TOKEN, refreshToken);
        editor.apply();
    }

    public static void saveAccessToken(Activity activity, String token){
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.TOKEN, token);
        editor.apply();
    }

    public static String getAccessToken(Activity activity){
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);

        return mSharedPreferences.getString(Constants.TOKEN,"");
    }

    public static String getRefreshToken(Activity activity){
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);

        return mSharedPreferences.getString(Constants.REFRESH_TOKEN,"");
    }

    public static String getEmail(Activity activity){
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);

        return mSharedPreferences.getString(Constants.EMAIL,"");
    }

    public static void resetTokens(Activity activity){
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.EMAIL,"");
        editor.putString(Constants.TOKEN,"");
        editor.putString(Constants.REFRESH_TOKEN,"");
        editor.apply();
    }

//    public static void logoutProcess(int code, String email, Context context, CompositeSubscription mSubscriptions) {
////        FCMtoken = FirebaseInstanceId.getInstance().getToken();
////        Log.d(TAG, "FCM token: " + FCMtoken);
//        if(code == 403) {
//            mContext = context;
//            String FCMtoken = "temp";
//            mSubscriptions.add(NetworkUtil.getRetrofit().logout(email, new User(FCMtoken))//Adding a dummy user to send FCM (otherwise, we cannot send strings)
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io())
//                    .subscribe(handleResponse, this::handleError));
//        }
//    }
//
//    public void handleResponse(Response response) {
//
//        Log.e("LOGOUT", "Logout succeeded!: " + response.toString());
//    }
//
//    public void handleError(Throwable error) {
//
//        Log.e("LOGOUT", "Logout error!: " + error.getMessage());
//        startActivity(context, new Intent(context, LogInActivity.class),null);
//
//        if (error instanceof HttpException) {
//
//            Gson gson = new GsonBuilder().create();
//
//            try {
//
//                String errorBody = ((HttpException) error).response().errorBody().string();
//                Response response = gson.fromJson(errorBody, Response.class);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } else {
//
//        }
//    }


}
