package com.eventime.eventime.utils;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.eventime.eventime.R;
import com.eventime.eventime.screens.events.CreateNewEventActivity;
import com.eventime.eventime.screens.events.EventEditFragment;
import com.eventime.eventime.screens.events.NewEventStepOneFragment;
import com.eventime.eventime.screens.users.ChangePasswordDialog;
import com.eventime.eventime.screens.users.EditProfileFragment;
import com.eventime.eventime.screens.users.LogInActivity;
import com.eventime.eventime.screens.users.RegisterStepOneFragment;

import org.apache.commons.compress.utils.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subscriptions.CompositeSubscription;

import static android.app.Activity.RESULT_OK;
import static com.eventime.eventime.utils.Constants.CAMERA_IMAGE_REQUEST;
import static com.eventime.eventime.utils.Constants.CAMERA_PERMISSIONS_REQUEST;
import static com.eventime.eventime.utils.Constants.GALLERY_IMAGE_REQUEST;
import static com.eventime.eventime.utils.Constants.GALLERY_PERMISSIONS_REQUEST;
import static com.facebook.FacebookSdk.getApplicationContext;

public class PictureDialog extends DialogFragment {

    public static final String TAG = PictureDialog.class.getSimpleName();

    private ChangePasswordDialog.Listener mListener;
    private CompositeSubscription mSubscriptions;
    private SharedPreferences mSharedPreferences;

    private Context globalContext;

    private Constants.Caller caller;

    public static final String FILE_NAME =  UUID.randomUUID().toString() + ".png";

    @BindView(R.id.b_take_photo)
    Button bTakePhoto;
    @BindView(R.id.b_gallery_pick)
    Button bGallery;

    @OnClick(R.id.b_take_photo)
    public void onTakePhoto(){
//        if (PermissionUtils.requestPermission(
//                getActivity(),
//                CAMERA_PERMISSIONS_REQUEST,
//                Manifest.permission.READ_EXTERNAL_STORAGE,
//                Manifest.permission.CAMERA)) {
//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            Log.e(TAG, globalContext.toString());
//            Log.e(TAG,getApplicationContext().getPackageName().toString());
//            Log.e("KUPA","KUPA");
//            Log.e(TAG,getCameraFile().toString());
//            Uri photoUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", getCameraFile());
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            startActivityForResult(intent, CAMERA_IMAGE_REQUEST);
//        }
        if (PermissionUtils.requestPermission(
                getActivity(),
                CAMERA_PERMISSIONS_REQUEST,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri photoUri = FileProvider.getUriForFile(getActivity(), getApplicationContext().getPackageName() + ".provider", getCameraFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(intent, CAMERA_IMAGE_REQUEST);
        }
    }
    @OnClick(R.id.b_gallery_pick)
    public void onGalleryPick() {
//        if (PermissionUtils.requestPermission(getActivity(), GALLERY_PERMISSIONS_REQUEST, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//            Intent intent = new Intent();
//            intent.setType("image/*");
//            Log.e("KUPA","KUPA Gallerowe");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//            startActivityForResult(Intent.createChooser(intent, "Select a photo"),
//                    GALLERY_IMAGE_REQUEST);
//
//        }
        if (PermissionUtils.requestPermission(getActivity(), GALLERY_PERMISSIONS_REQUEST, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select a photo"),
                    GALLERY_IMAGE_REQUEST);
        }
    }

    public File getCameraFile() {
        File dir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(dir, FILE_NAME);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult");
        if (requestCode == GALLERY_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            Log.d(TAG, "onActivityResult2");
            Log.d(TAG, FILE_NAME);
            Log.d(TAG, data.getData().toString());
            //setBitmap(data.getData());
            uploadImage(data.getData());
        } else if (requestCode == CAMERA_IMAGE_REQUEST && resultCode == RESULT_OK) {
            Log.d(TAG, "onActivityResult3");
            Log.d(TAG, FILE_NAME);
            Uri uri = FileProvider.getUriForFile(getActivity(), getApplicationContext().getPackageName() + ".provider", getCameraFile());
            Log.d(TAG, uri.toString());
            //setBitmap(uri);
            uploadImage(uri);
        }

        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == GALLERY_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
//            uploadImage(data.getData());
//        } else if (requestCode == CAMERA_IMAGE_REQUEST && resultCode == RESULT_OK) {
//            Uri photoUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", getCameraFile());
//            uploadImage(photoUri);
//        }
    }

    public void uploadImage(Uri uri) {
        if (uri != null) {
            try {
                // scale the image to save on bandwidth
                Bitmap bitmap =
                        scaleBitmapDown(
                                MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri),
                                800);


                Log.e("URI NOW", uri+"");
                uri = Uri.parse(getFilePathFromURI(getActivity(),uri));
                Log.e("URI NOW2", uri+"");
                bitmap = ImagesHandler.rotate(uri.toString(), bitmap);

                if(Constants.Caller.NEW_PROFILE==caller){
                    RegisterStepOneFragment.ibUserImage.setImageBitmap(bitmap);
                    Log.d(TAG, "caller new profile");
                    Log.d(TAG, uri.toString());
                  RegisterStepOneFragment.pictureUri=uri;//Uri.parse(getRealPathFromUri(getContext(),uri));
                  RegisterStepOneFragment.user.setPicture(Constants.USER_PICTURE_BUCKET+FILE_NAME);
                  RegisterStepOneFragment.pictureName = FILE_NAME;
                  LogInActivity.picBitmap = bitmap;//MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                }
                else if(Constants.Caller.NEW_EVENT==caller){
                    Log.d(TAG, "caller new event");
                    NewEventStepOneFragment.ibEventImage.setImageBitmap(bitmap);
                    CreateNewEventActivity.myEvent.setImage(Constants.EVENT_PICTURE_BUCKET+FILE_NAME);
                    CreateNewEventActivity.picBitmap = bitmap;//MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
                    CreateNewEventActivity.picUri=uri;
                    CreateNewEventActivity.picName=FILE_NAME;
                }
                else if(Constants.Caller.EDIT_EVENT==caller){
                    Log.d(TAG, "caller edit event");
                    EventEditFragment.ibEventImage.setImageBitmap(bitmap);
                    EventEditFragment.pictureUri = uri;
                }
                else if(Constants.Caller.EDIT_PROFILE==caller){
                    Log.d(TAG, "caller edit proile");
                    EditProfileFragment.ivUserImage.setImageBitmap(bitmap);
                    EditProfileFragment.pictureUri = uri;
                }
//                else if(Constants.Caller.NEW_ORGANIZATION==caller){
//                    Log.d(TAG, "caller new organization");
//                    NewOrganizationStepOneFragment.ibOrgImage.setImageBitmap(bitmap);
//                    NewOrganizationActivity.picBitmap=bitmap;
//                    NewOrganizationActivity.picName=FILE_NAME;
//                    NewOrganizationActivity.picUri=uri;
//                }
                getDialog().dismiss();

            } catch (IOException e) {
                Log.d(TAG, "Image picking failed because " + e.getMessage());
                Toast.makeText(getActivity(), R.string.image_picker_error, Toast.LENGTH_LONG).show();
            }
        } else {
            Log.d(TAG, "Image picker gave us a null image.");
            Toast.makeText(getActivity(), R.string.image_picker_error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onPermissionsResult");
        switch (requestCode) {
            case CAMERA_PERMISSIONS_REQUEST:
                if (PermissionUtils.permissionGranted(requestCode, CAMERA_PERMISSIONS_REQUEST, grantResults)) {
                    onTakePhoto();
                }
                break;
            case GALLERY_PERMISSIONS_REQUEST:
                if (PermissionUtils.permissionGranted(requestCode, GALLERY_PERMISSIONS_REQUEST, grantResults)) {
                    onGalleryPick();
                }
                break;
        }
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Log.e("My Dialog","1");
        Cursor cursor = null;
        try {
            Log.e("My Dialog","2");
            String[] proj = { MediaStore.Images.Media.DATA };
            Log.e("My Dialog","3");
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            Log.e("My Dialog","4");
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            Log.e("My Dialog","5");
            cursor.moveToFirst();
            Log.e("My Dialog","6");
            return cursor.getString(column_index);

        } finally {
            Log.e("My Dialog","7");
            if (cursor != null) {
                Log.e("My Dialog","8");
                cursor.close();

            }
        }
    }

//    public String getAbsolutePath(Uri uri) {
//        String[] projection = { MediaStore.MediaColumns.DATA };
//        @SuppressWarnings("deprecation")
//        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
//        if (cursor != null) {
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        } else
//            return null;
//    }

//    public String getPath(Uri uri) {
//
//        try{
//            String[] projection = { MediaStore.Images.Media.DATA };
//            Log.e("OK 1",""+projection);
//            Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
//            Log.e("OK 2",""+cursor);
//            if(cursor==null)
//            {
//                Log.e("OK is null","IS NULL");
//                return null;
//
//            }
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//            Log.e("OK 3",""+column_index);
//            cursor.moveToFirst();
//            Log.e("OK 4",""+cursor.getString(column_index));
//            return cursor.getString(column_index);
//        }
//        catch(Exception e)
//        {
//            Log.e(TAG,"NULL");
//        }
////            Toast.makeText(PhotoActivity.this, "Image is too big in resolution please try again", 5).show();
//            return null;
//        }

    public static String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        Log.e("FILE NAME", fileName);
        Log.e("FILE PATH", contentUri.toString());
        Log.e("FILE TEMP PATH", String.valueOf(context.getCacheDir()) + File.separator + fileName);
        if (!TextUtils.isEmpty(fileName)) {
            File copyFile = new File(String.valueOf(context.getCacheDir())+ File.separator + fileName);//"TEMP PATH" + File.separator + fileName);
            copy(context, contentUri, copyFile);
            Log.e("Absolute path", copyFile.getAbsolutePath());
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Bitmap scaleBitmapDown(Bitmap bitmap, int maxDimension) {

        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int resizedWidth = maxDimension;
        int resizedHeight = maxDimension;

        if (originalHeight > originalWidth) {
            resizedHeight = maxDimension;
            resizedWidth = (int) (resizedHeight * (float) originalWidth / (float) originalHeight);
        } else if (originalWidth > originalHeight) {
            resizedWidth = maxDimension;
            resizedHeight = (int) (resizedWidth * (float) originalHeight / (float) originalWidth);
        } else if (originalHeight == originalWidth) {
            resizedHeight = maxDimension;
            resizedWidth = maxDimension;
        }
        Log.d("RESIZED","yes");
        return Bitmap.createScaledBitmap(bitmap, resizedWidth, resizedHeight, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_photo_selector,container,false);
        mSubscriptions = new CompositeSubscription();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        ButterKnife.bind(this, view);
        caller =(Constants.Caller) getArguments().getSerializable("caller");
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        globalContext = context;
//        mListener = (CreateNewEventActivity)context;
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        context=activity;
//    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // below line to be commented to prevent crash on nougat.
        // http://blog.sqisland.com/2016/09/transactiontoolargeexception-crashes-nougat.html
        //
        //super.onSaveInstanceState(outState);
    }


}