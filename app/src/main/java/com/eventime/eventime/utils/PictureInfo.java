package com.eventime.eventime.utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Tomek on 2017-08-23.
 */

public class PictureInfo implements Parcelable{
    String pictureUri;

    public String getPictureUri() {
        return pictureUri;
    }

    public void setPictureUri(String pictureUri) {
        this.pictureUri = pictureUri;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    String userPicture;

    public PictureInfo(String pictureUri, String userPicture){
        this.pictureUri=pictureUri;
        this.userPicture=userPicture;
    }

    protected PictureInfo(Parcel in) {
        pictureUri = in.readString();
        userPicture = in.readString();
    }

    public static final Creator<PictureInfo> CREATOR = new Creator<PictureInfo>() {
        @Override
        public PictureInfo createFromParcel(Parcel in) {
            return new PictureInfo(in);
        }

        @Override
        public PictureInfo[] newArray(int size) {
            return new PictureInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userPicture);
        dest.writeString(pictureUri);
    }
}
