package com.eventime.eventime.network;

//import com.ballotbox.ballotbox.model.Comment;
//import com.ballotbox.ballotbox.model.CommentList;
//import com.ballotbox.ballotbox.model.DebateList;
import com.eventime.eventime.model.Event;
//import com.ballotbox.ballotbox.model.EventList;
//import com.ballotbox.ballotbox.model.Message;
//import com.ballotbox.ballotbox.model.MessageList;

import com.eventime.eventime.model.EventList;
import com.eventime.eventime.model.Response;
import com.eventime.eventime.model.User;
//import com.eventime.eventime.model.UserList;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

public interface RetrofitInterface {

    //USERS
    @POST("users/signup")
    Observable<Response> register(@Body User user);

    @POST("users/login")
    Observable<Response> login(@Body User user);

    @POST("users/relogin")
    Observable<Response> reLogin();

    @GET("users/{email}")
    Observable<User> getProfile(@Path("email") String email);

    @POST("users/edit/{email}")
    Observable<Response> editProfile(@Path("email") String email, @Body User user);

    @PUT("users/{email}")
    Observable<Response> changePassword(@Path("email") String email, @Body User user);

    @POST("users/{email}/password")
    Observable<Response> resetPasswordInit(@Path("email") String email);

    @POST("users/{email}/password")
    Observable<Response> resetPasswordFinish(@Path("email") String email, @Body User user);

    @POST("users/fb/login/{email}")
    Observable<Response> checkIfUserRegistered(@Path("email") String email, @Body User user);

    @POST("users/fb/signup")
    Observable<Response> registerFb(@Body User user);

    @POST("users/logout/{email}")
    Observable<Response> logout(@Path("email") String email, @Body User user);

    @GET("users/getPicture/{email}")
    Observable<User> getPicture(@Path("email") String email);

    @POST("users/checkEmail/{email}")
    Observable<Response> checkEmail(@Path("email") String email);

//    @POST("users/search/{query}")
//    Observable<UserList> searchUsers(@Path("query") String query);


    //EVENTS
    @POST("events/create/{email}")
    Observable<Response> createEvent(@Path("email") String email, @Body Event event);

    @POST("events/edit/{id}")
    Observable<Response> editEvent(@Path("id") String id, @Body Event event); //Send only data which has changed

    @GET("events/delete/{id}")
    Observable<Response> deleteEvent(@Path("id") String id, @Body Event event);

    @GET("events/getEvents/{email}")
    Observable<EventList> getEvents(@Path("email") String email);

    @GET("events/joinedEvents/{email}")
    Observable<EventList> joinedEvents(@Path("email") String email);

    @GET("events/myEvents/{email}")
    Observable<EventList> myEvents(@Path("email") String email);

    @GET("events/interestedEvents/{email}")
    Observable<EventList> interestedEvents(@Path("email") String email);

    @POST("events/join/{id}/{email}")
    Observable<Response> joinEvent(@Path("id") String id, @Path("email") String email);

    @POST("events/unjoin/{id}/{email}")
    Observable<Response> unjoinEvent(@Path("id") String id, @Path("email") String email);

    @POST("events/interested/{id}/{email}")
    Observable<Response> interestedinEvent(@Path("id") String id, @Path("email") String email);

    @POST("events/uninterested/{id}/{email}")
    Observable<Response> uninterestedinEvent(@Path("id") String id, @Path("email") String email);

//    @GET("events/getComments/{id}") //event id
//    Observable<CommentList> getEventComments(@Path("id") String id);
//
    @GET("events/search/{query}")
    Observable<EventList> getEventsSearch(@Path("query") String query);
//
//    @GET("events/getUsersAttending/{id}")
//    Observable<UserList> getUsersAttending(@Path("id") String id);
//
//    @GET("events/getUsersInterested/{id}")
//    Observable<UserList> getUsersInterested(@Path("id") String id);

    @POST("events/filteredEvents")
    Observable<EventList> getFilteredEvents(@Body Event event);


    //Comments
//    @POST("comments/create/{email}")
//    Observable<Response> createComment(@Path("email") String email, @Body Comment comment);
//
//    @POST("comments/edit/{id}")// comment id
//    Observable<Response> editComment(@Path("id") String id, @Body Comment comment);
//
//    @GET("comments/delete/{id}") //comment id
//    Observable<Response> deleteComment(@Path("id") String id, @Body Comment comment);
//
//    @GET("comments/getResponses/{id}") //comment id
//    Observable<CommentList> getCommentResponses(@Path("id") String id);

    //MESSAGES
//    @POST("messages/getMessages")
//    Observable<MessageList> getMessages(@Body Message message);
//
//    @GET("messages/getLastConversation/{email}")
//    Observable<MessageList> getLastConversation(@Path("email") String email);

}
